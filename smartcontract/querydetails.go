// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"errors"

	"github.com/ethereum/go-ethereum/common"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
)

var QueryUnknownError error = errors.New("No query with this query ID")

// GetQueryDetails returns any log entries corresponding to the given query ID.
// If no query is found that corresponds to the given ID, and an error is returned.
// Otherwise, the first return value contains the approval event, and the second
// value may be nil or contain the completion event.
func (scc *Coordinator) GetQueryDetails(queryid string) (coordinator.QueryDetails, error) {
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()

	var queryDetails coordinator.QueryDetails
	for i := range scc.auditLogs.Entries {
		switch scc.auditLogs.Entries[i].Kind {
		case coordinator.QueryApproved:
			if queryid == scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryApproved).Query.ID {
				queryDetails.LogEntryQueryApproved = new(coordinator.LogEntryQueryApproved)
				*queryDetails.LogEntryQueryApproved = scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryApproved)
			}
		case coordinator.QueryManualReview:
			if queryid == common.Hash(scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryManualReview).QueryID).Hex()[2:] {
				queryDetails.LogEntryQueryManualReview = new(coordinator.LogEntryQueryManualReview)
				*queryDetails.LogEntryQueryManualReview = scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryManualReview)
			}
		case coordinator.QueryComplete:
			if queryid == common.Hash(scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryComplete).QueryID).Hex()[2:] {
				queryDetails.LogEntryQueryComplete = new(coordinator.LogEntryQueryComplete)
				*queryDetails.LogEntryQueryComplete = scc.auditLogs.Entries[i].Contents.(coordinator.LogEntryQueryComplete)
			}
		}

		if queryDetails.LogEntryQueryApproved != nil && queryDetails.LogEntryQueryComplete != nil {
			break
		}
	}

	if queryDetails.LogEntryQueryApproved == nil {
		return queryDetails, QueryUnknownError
	}

	return queryDetails, nil
}
