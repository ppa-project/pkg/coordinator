// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"math/big"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

func TestAuditLogApi(t *testing.T) {
	assert := assert.New(t)

	sc := &Coordinator{
		auditLogs: AuditLogs{
			Entries: []coordinator.LogEntry{
				{
					Kind:      coordinator.PresenceGUI,
					Timestamp: time.Unix(0, 0),
					Contents:  0,
				},
				{
					Kind:      coordinator.QueryApproved,
					Timestamp: time.Unix(1, 0),
					Contents:  1,
				},
				{
					Kind:      coordinator.QueryComplete,
					Timestamp: time.Unix(2, 0),
					Contents:  2,
				},
				{
					Kind:      coordinator.PresenceMPC,
					Timestamp: time.Unix(3, 0),
					Contents:  3,
				},
				{
					Kind:      coordinator.PresenceGUI,
					Timestamp: time.Unix(4, 0),
					Contents:  4,
				},
				{
					Kind:      coordinator.QueryApproved,
					Timestamp: time.Unix(5, 0),
					Contents:  5,
				},
			},
		},
	}

	assert.Equal(3, sc.AuditLogLen(coordinator.PresenceGUI|coordinator.PresenceMPC))
	assert.Equal(0, sc.AuditLogLen(coordinator.QueryRejected))
	assert.Equal(6, sc.AuditLogLen(coordinator.Any))

	assert.Equal(3, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 0, 10)))
	assert.Equal(0, len(sc.AuditLog(coordinator.QueryRejected, 0, 10)))
	assert.Equal(6, len(sc.AuditLog(coordinator.Any, 0, 10)))

	assert.Equal(3, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 0, 0)))
	assert.Equal(2, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 1, 0)))
	assert.Equal(1, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 0, 1)))
	assert.Equal(1, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 1, 1)))
	assert.Equal(1, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 2, 1)))
	assert.Equal(0, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 3, 1)))
	assert.Equal(0, len(sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 7, 10)))

	assert.Equal(
		[]coordinator.LogEntry{
			{
				Kind:      coordinator.PresenceGUI,
				Timestamp: time.Unix(0, 0),
				Contents:  0,
			},
			{
				Kind:      coordinator.PresenceMPC,
				Timestamp: time.Unix(3, 0),
				Contents:  3,
			},
			{
				Kind:      coordinator.PresenceGUI,
				Timestamp: time.Unix(4, 0),
				Contents:  4,
			},
		},
		sc.AuditLog(coordinator.PresenceGUI|coordinator.PresenceMPC, 0, 10),
	)
}

// Test if node config updates are visible in the audit log
func TestAuditLogRetrieval(t *testing.T) {
	assert := assert.New(t)
	Setup()
	// Add a node that exists when we start up
	scPresence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := scPresence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.Nil(err, "Can't add new admin to presence")
	client.Commit()

	_, err = scPresence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{
		Organization: "TestPartyA",
		Ip:           "111",
		Port:         "115",
		Cert:         "222",
		Paillierkey:  "333",
		DbHash:       "555",
	})
	assert.Nil(err, "Can't register new node")
	client.Commit()
	_, err = scPresence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.Nil(err, "Can't approve new node")
	client.Commit()

	// Add a query event that exists when we start
	scMaintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err = scMaintenance.SetPrivacyBudget(genesisAccount, "TestPartyA", big.NewInt(100000))
	assert.Nil(err, "Can't set privacy budget")
	scQuery, _ := sc.NewQuery(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err = scQuery.SubmitQuery(genesisAccount, sc.PPAQueryQuery{
		Columns: []sc.PPAQueryColumn{{
			Func:      "SUM",
			Owner:     "A",
			Attribute: "A",
		}},
		Constraints: []sc.PPAQueryConstraint{{
			Owner:          "A",
			Attribute:      "A",
			Operator:       "==",
			ReferenceValue: "12",
		}},
	}, sc.PPAQueryUserMetadata{})
	assert.Nil(err, "Can't submit new query")
	client.Commit()

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should have received a node update
	assert.Equal(1, c.AuditLogLen(coordinator.PresenceMPC))
	assert.Equal(1, c.AuditLogLen(coordinator.QueryApproved))

	// Send another node update
	_, err = scPresence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{
		Organization: "TestPartyA",
		Ip:           "101",
		Port:         "105",
		Cert:         "202",
		Paillierkey:  "303",
		DbHash:       "505",
	})
	assert.Nil(err, "Can't register new node again")
	client.Commit()

	// Send query manual review
	_, err = scQuery.SubmitManualReview(genesisAccount, [32]byte{2}, "Contoso")
	assert.Nil(err, "Can't submit review event")
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// Send query completion
	_, err = scQuery.SubmitResult(genesisAccount, [32]byte{2}, "hello")
	assert.Nil(err, "Can't submit result")
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should receive this update too
	assert.Equal(2, c.AuditLogLen(coordinator.PresenceMPC), "Wrong number of MPC presence logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryApproved), "Wrong number of Query Approved logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryManualReview), "Wrong number of Query in Review logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryComplete), "Wrong number of Query Completed logs")
	assert.Equal(2, c.AuditLogLen(coordinator.QueryApproved|coordinator.QueryComplete), "Wrong number of Query Approved / Completed logs")
	assert.Equal(5, c.AuditLogLen(coordinator.Any), "Wrong total number of logs")

	// Inspect the query complete log entry
	entry := c.AuditLog(coordinator.QueryComplete, 0, 0)[0]
	assert.Equal(coordinator.QueryComplete, entry.Kind, "Log entry has wrong kind")
	assert.False(time.Unix(0, 0).Equal(entry.Timestamp), "Log entry has no timestamp")
	assert.Equal(
		coordinator.LogEntryQueryComplete{
			Sender:     genesisAccount.From,
			QueryID:    [32]byte{2},
			Successful: true,
			ResultHash: "hello",
		},
		entry.Contents,
		"Log entry contents are incorrect",
	)

	// Disconnect from the smartcontract and reconnect
	c.Close()
	time.Sleep(100 * time.Millisecond)
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	time.Sleep(100 * time.Millisecond)

	// See if the logs are still there
	assert.Equal(2, c.AuditLogLen(coordinator.PresenceMPC), "Wrong number of MPC presence logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryApproved), "Wrong number of Query Approved logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryManualReview), "Wrong number of Query in Review logs")
	assert.Equal(1, c.AuditLogLen(coordinator.QueryComplete), "Wrong number of Query Completed logs")
	assert.Equal(2, c.AuditLogLen(coordinator.QueryApproved|coordinator.QueryComplete), "Wrong number of Query Approved / Completed logs")
	assert.Equal(5, c.AuditLogLen(coordinator.Any), "Wrong total number of logs")
}
