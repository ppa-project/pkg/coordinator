// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// Test if you can await transactions successfully
func TestAwaitTransaction(t *testing.T) {
	assert := assert.New(t)
	Setup()

	scc := NewCoordinator(*testConfig)
	scc.SetClient(client)
	if scc.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer scc.Close()
	client.Commit()

	go clientCommitAfter(time.Second / 2)
	scc.ensureFunds(context.Background())

	// A transaction that should pass immediately, so we give it a timeout of 1s and expect no error
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	go clientCommitAfter(time.Second / 2)
	assert.NoError(scc.makeAndAwaitTransaction(ctx), "Transaction should have passed immediately")

	cancel()
	time.Sleep(time.Second)

	// A transaction that should pass slowly, so we give it a timeout of 0.5s and expect failure
	ctx, cancel = context.WithTimeout(context.Background(), time.Second/2)
	go clientCommitAfter(time.Second)
	if err := scc.makeAndAwaitTransaction(ctx); assert.Error(err, "Transaction should have timed out") {
		assert.Equal("context deadline exceeded", err.Error())
	}

	cancel()
	time.Sleep(time.Second)

	// A transaction that should fail to pass, we cancel it manually, so we give it a timeout of 0.5s and expect failure
	ctx, cancel = context.WithCancel(context.Background())
	go func() { time.Sleep(time.Second / 2); cancel() }()
	if err := scc.makeAndAwaitTransaction(ctx); assert.Error(err, "Transaction should have been canceled") {
		assert.Equal("context canceled", err.Error())
	}

	client.Commit()
	time.Sleep(time.Second)
}

func (scc *Coordinator) makeAndAwaitTransaction(ctx context.Context) error {
	tx, err := scc.smartContract.PushButton(scc.wallet)
	if err != nil {
		return err
	}
	return scc.awaitTransactionNoCommit(ctx, tx.Hash())
}

// Helper function to test awaiting
func clientCommitAfter(d time.Duration) {
	time.Sleep(d)
	client.Commit()
}
