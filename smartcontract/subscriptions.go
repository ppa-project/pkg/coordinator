// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"github.com/rs/zerolog/log"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/event"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

// Creates channels from which new events will come
func (scc *Coordinator) subscribeEvents() error {
	// Set up the various event listeners
	var firstBlockNr uint64 = 1
	opts := &bind.WatchOpts{Start: &firstBlockNr}

	// Set up the listener for incoming node registration events
	mpcNodeRegisteredChannel := make(chan *sc.PresenceMPCNodeRegistered)
	mpcNodeRegisteredSubscription, err := scc.smartContract.WatchMPCNodeRegistered(opts, mpcNodeRegisteredChannel)
	if err != nil {
		log.Err(err).Msgf("Unable to subscribe to node registration events")
		return err
	}
	scc.subscriptions = append(scc.subscriptions, mpcNodeRegisteredSubscription)

	// Set up the listener for incoming GUI registration events
	mpcGuiRegisteredChannel := make(chan *sc.PresenceGUINodeRegistered)
	mpcGuiRegisteredSubscription, err := scc.smartContract.WatchGUINodeRegistered(opts, mpcGuiRegisteredChannel)
	if err != nil {
		log.Err(err).Msgf("Unable to subscribe to GUI registration events")
		return err
	}
	scc.subscriptions = append(scc.subscriptions, mpcGuiRegisteredSubscription)

	// Set up the listener for incoming query events
	queryEventChannel := make(chan *sc.QueryQueryApproved)
	queryEventSubscription, err := scc.smartContract.WatchQueryApproved(opts, queryEventChannel)
	if err != nil {
		log.Err(err).Msgf("Unable to subscribe to new query events")
		return err
	}
	scc.subscriptions = append(scc.subscriptions, queryEventSubscription)

	// Start listening to the subscription and error channels
	go func() {
		for {
			select {
			// Listen to all of the event channels, dispatching the events to their listeners
			case newNodeEvent := <-mpcNodeRegisteredChannel:
				go scc.dispatchNewNodeConfig(newNodeEvent)
			case newGuiEvent := <-mpcGuiRegisteredChannel:
				go scc.dispatchNewGuiConfig(newGuiEvent)
			case newQueryEvent := <-queryEventChannel:
				go scc.dispatchNewQuery(newQueryEvent)

			// If an error channel is closed, handle the connection error
			case err := <-mpcNodeRegisteredSubscription.Err():
				scc.connectionError(err)
				return
			case err := <-mpcGuiRegisteredSubscription.Err():
				scc.connectionError(err)
				return
			case err := <-queryEventSubscription.Err():
				scc.connectionError(err)
				return
			}
		}
	}()

	return nil
}

// When starting up, we need each node's configuration immediately.
// dispatchMostRecentNodeConfigEvents searches the past logs and reports the latest one for each org
func (scc *Coordinator) dispatchMostRecentNodeConfigEvents() error {
	log.Debug().Msg("Enumerating past node registration events")

	// Set up a filtering event iterator
	iter, err := scc.smartContract.FilterMPCNodeRegistered(&bind.FilterOpts{Start: 1})
	if err != nil {
		log.Err(err).Msg("Unable to access past node registration events")
		return err
	}
	defer iter.Close()

	// Find and save the most recent config update for each org
	configs := make(map[string]*sc.PresenceMPCNodeRegistered)
	for iter.Next() {
		configs[iter.Event.NodeData.Organization] = iter.Event
	}
	if iter.Error() != nil {
		log.Err(iter.Error()).Msg("An error occurred while accessing past node registration events")
		return iter.Error()
	}

	// Call the node presence listeners once for each org
	for _, data := range configs {
		scc.dispatchNewNodeConfig(data)
	}
	return nil
}

// When starting up, we need our GUI's configuration immediately.
// dispatchMostRecentGuiConfigEvents searches the past logs and reports the most recent one
func (scc *Coordinator) dispatchMostRecentGuiConfigEvents() error {
	log.Debug().Msg("Enumerating past GUI registration events")

	// Set up a filtering event iterator
	iter, err := scc.smartContract.FilterGUINodeRegistered(&bind.FilterOpts{Start: 1})
	if err != nil {
		log.Err(err).Msg("Unable to access past GUI registration events")
		return err
	}
	defer iter.Close()

	// Find and save the most recent config update for each org
	var config *sc.PresenceGUINodeRegistered
	for iter.Next() {
		if iter.Event.NodeData.Organization == scc.config.Organization {
			config = iter.Event
		}
	}
	if iter.Error() != nil {
		log.Err(iter.Error()).Msg("An error occurred while accessing past GUI registration events")
		return iter.Error()
	}
	if config == nil {
		log.Warn().Msg("No previous GUI configuration was found on the blockchain")
		return nil
	}

	scc.dispatchNewGuiConfig(config)
	return nil
}

// Feeds a new node event to each of the registered listeners
func (scc *Coordinator) dispatchNewNodeConfig(newNodeEvent *sc.PresenceMPCNodeRegistered) {
	scc.Lock()
	defer scc.Unlock()
	for i := range scc.presenceListeners {
		scc.presenceListeners[i](
			newNodeEvent.NodeData.Organization,
			newNodeEvent.NodeData.Ip,
			newNodeEvent.NodeData.Port,
			newNodeEvent.NodeData.Cert,
			newNodeEvent.NodeData.Paillierkey,
		)
	}
}

// Feeds a new GUI event to each of the registered listeners
func (scc *Coordinator) dispatchNewGuiConfig(newGuiEvent *sc.PresenceGUINodeRegistered) {
	if newGuiEvent.NodeData.Organization != scc.config.Organization {
		return
	}
	scc.Lock()
	defer scc.Unlock()
	for i := range scc.guiListeners {
		scc.guiListeners[i](
			newGuiEvent.NodeData.Ip,
			newGuiEvent.NodeData.Cert,
		)
	}
}

// Feeds a new query event to each of the registered listeners
func (scc *Coordinator) dispatchNewQuery(newQueryEvent *sc.QueryQueryApproved) {
	qid := common.Bytes2Hex(newQueryEvent.QueryID[:])

	// Build the slice of PercentageConstraints
	percentageConstraints := make([]coordinator.PercentageConstraint, 0, len(newQueryEvent.Metadata.PercentageConstraints))
	for i := range newQueryEvent.Metadata.PercentageConstraints {
		percentageConstraints = append(percentageConstraints, PPAQueryPercentageConstraint(newQueryEvent.Metadata.PercentageConstraints[i]).PercentageConstraint())
	}

	// Build the slice of StdevConstraints
	stdevConstraints := make([]coordinator.StdevConstraint, 0, len(newQueryEvent.Metadata.StdevConstraints))
	for i := range newQueryEvent.Metadata.StdevConstraints {
		stdevConstraints = append(stdevConstraints, PPAQueryStdevConstraint(newQueryEvent.Metadata.StdevConstraints[i]).StdevConstraint())
	}

	options := map[string]interface{}{
		"minResultSize":         newQueryEvent.Metadata.MinResultSize.Int64(),
		"minResultPercentage":   newQueryEvent.Metadata.MinResultPercentage.Int64(),
		"maxResultPercentage":   newQueryEvent.Metadata.MaxResultPercentage.Int64(),
		"destinationNodes":      newQueryEvent.Metadata.DestinationNodes,
		"stdevConstraints":      stdevConstraints,
		"percentageConstraints": percentageConstraints,
		"groupkey":              newQueryEvent.Metadata.Groupkey,
		"reviewer":              newQueryEvent.Metadata.Reviewer,
	}

	scc.Lock()
	defer scc.Unlock()
	for i := range scc.queryListeners {
		scc.queryListeners[i](
			"basicstats", // TODO: should this be dynamic?
			qid,
			PPAQueryQuery(newQueryEvent.Query).TypesQuery(),
			options,
		)
	}
}

// Unsubscribe from all event channels. Assumes Lock()ed.
func (scc *Coordinator) unsubscribeAll() {
	for i := range scc.subscriptions {
		scc.subscriptions[i].Unsubscribe()
	}
	scc.subscriptions = make([]event.Subscription, 0)
}
