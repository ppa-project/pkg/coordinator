// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"math/big"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/accounts/abi/bind/backends"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/crypto"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/test"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

func TestMain(m *testing.M) {
	test.Setup()
	os.Exit(m.Run())
}

func TestImplementsCoordinator(t *testing.T) {
	var _ coordinator.Coordinator = &Coordinator{}
}

// TestConnection wraps a simulated backend, which somehow does not
// implement the required function NetworkID.
type TestConnection struct {
	backends.SimulatedBackend
}

// Returns the Network ID. For some reason, this is an undocumented 1337
// for the simulated backend.
func (tc *TestConnection) NetworkID(context.Context) (*big.Int, error) {
	return big.NewInt(1337), nil
}

// The simulated backend's close interface differs from the real one...
// It works differently too: a simulated backend can not reopen the connection
// once closed. This method is a no-op for that reason.
func (tc *TestConnection) Close() {
	//_ = tc.SimulatedBackend.Close()
}

var client *TestConnection
var testConfig *coordinator.Config
var genesisAccount *bind.TransactOpts

// Setup starts a simulated ethereum blockchain and deploys the smart
// contracts.
func Setup() {
	// Ethereum tests setup
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		log.Fatal().Err(err).Msg("Can't generate a key")
	}

	genesisAccount = bind.NewKeyedTransactor(privateKey)

	balance := big.NewInt(10)
	balance = balance.Exp(balance, big.NewInt(69), nil)

	genesisAlloc := map[common.Address]core.GenesisAccount{
		genesisAccount.From: {Balance: balance},
	}

	blockGasLimit := uint64(20000000)
	client = &TestConnection{*backends.NewSimulatedBackend(genesisAlloc, blockGasLimit)}
	// To mine submitted transactions and see the results (events, state), call
	// client.Commit()

	// Deploy the smart contracts
	// DiamondStart sets up the diamond contract with basic ownership and the facet cutter
	diamondAddr, _, _, err := sc.DeployDiamondStart(genesisAccount, client)
	if err != nil {
		log.Fatal().Err(err).Msg("Can't deploy the testing diamond start contract")
	}
	diamondFacet, err := sc.NewDiamondFacet(diamondAddr, client)
	if err != nil {
		log.Fatal().Err(err).Msg("Can't instantiate the testing diamond facet contract")
	}
	client.Commit()

	// Deploy the presence and query contracts
	pAddr, _, _, err := sc.DeployPresence(genesisAccount, client)
	if err != nil {
		log.Fatal().Err(err).Msg("Can't deploy the testing presence contract")
	}
	qAddr, _, _, err := sc.DeployQuery(genesisAccount, client)
	if err != nil {
		log.Fatal().Err(err).Msg("Can't deploy the testing query contract")
	}
	mAddr, _, _, err := sc.DeployMaintenance(genesisAccount, client)
	if err != nil {
		log.Fatal().Err(err).Msg("Can't deploy the testing maintenance contract")
	}

	// Start making the diamond cut
	diamondCut := make([][]byte, 3)
	diamondCut[0] = pAddr.Bytes()
	diamondCut[1] = qAddr.Bytes()
	diamondCut[2] = mAddr.Bytes()

	// For the diamond cut, we need all the method signature hashes (four bytes).
	// These are helpfully printed in the generated source code in presence.go and
	// query.go.
	for i, signatures := range [][]string{sc.METHODS_PRESENCE, sc.METHODS_QUERY, sc.METHODS_MAINTENANCE} {
		for _, signature := range signatures {
			bytes, _ := hex.DecodeString(signature)
			diamondCut[i] = append(diamondCut[i], bytes...)
		}
	}

	_, err = diamondFacet.DiamondCut(genesisAccount, diamondCut)
	if err != nil {
		log.Fatal().Err(err).Msg("DiamondCut failed")
	}
	client.Commit()

	// Register the account with ethers as the volunteer
	presence, _ := sc.NewPresence(diamondAddr, client)
	_, err = presence.NewVolunteerPrivateKey(genesisAccount, privateKey.D.Text(16))
	if err != nil {
		log.Fatal().Err(err).Msg("Can't register volunteer key")
	}
	client.Commit()

	testConfig = &coordinator.Config{
		Organization:            "PartyA",
		Ip:                      "123.45.67.89",
		Port:                    "443",
		Cert:                    "---TLS---",
		DbHash:                  "abcd1234",
		EthereumPrivateKey:      "fad9c8855b740a0b7ed4c221dbad0f33a83a49cad6b3fe8d5817ac83d38b6a19",
		EthereumURL:             "invalidurl",
		EthereumContractAddress: diamondAddr.Hex(),
	}
}

// Test if NewCoordinator returns a correct instance
func TestNew(t *testing.T) {
	Setup()
	c := NewCoordinator(*testConfig)
	assert.NotNil(t, c.logsIn)
	assert.EqualValues(t, c.config, *testConfig)
}

func TestConnectAndListen(t *testing.T) {
	Setup()
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	err := c.ConnectAndListen()
	assert.Nil(t, err)
	c.Close()
}

func TestFindSmartContract(t *testing.T) {
	Setup()
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	addr := c.FindFirstContractCreation()
	assert.NotNil(t, addr)
	assert.Equal(t, testConfig.EthereumContractAddress, addr.Hex())
	addr = c.FindFirstDiamondCutEvent(context.Background())
	assert.NotNil(t, addr)
	assert.Equal(t, testConfig.EthereumContractAddress, addr.Hex())
	c.Close()
}

func TestFindNoSmartContract(t *testing.T) {
	// instead of Setup, we start a blockchain *without* a smart contract
	// Ethereum tests setup
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		log.Fatal().Err(err).Msg("Can't generate a key")
	}

	genesisAccount = bind.NewKeyedTransactor(privateKey)

	balance := big.NewInt(10)
	balance = balance.Exp(balance, big.NewInt(69), nil)

	genesisAlloc := map[common.Address]core.GenesisAccount{
		genesisAccount.From: {Balance: balance},
	}

	blockGasLimit := uint64(20000000)
	client = &TestConnection{*backends.NewSimulatedBackend(genesisAlloc, blockGasLimit)}
	// To mine submitted transactions and see the results (events, state), call
	// client.Commit()
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	addr := c.FindFirstContractCreation()
	assert.Nil(t, addr)
	addr = c.FindFirstDiamondCutEvent(context.Background())
	assert.Nil(t, addr)
	c.Close()
}

type mockListener struct {
	sync.Mutex
	protocolName                     string
	queryid                          string
	query                            interface{}
	options                          map[string]interface{}
	org, ip, port, cert, paillierkey string
}

func (ml *mockListener) IncomingQuery(protocolName, queryid string, query interface{}, options map[string]interface{}) {
	ml.Lock()
	defer ml.Unlock()
	ml.protocolName = protocolName
	ml.queryid = queryid
	ml.query = query
	ml.options = options
	log.Debug().Msg("Called IncomingQuery")
}

func (ml *mockListener) NodeConfigurationUpdate(org, ip, port, cert, paillierkey string) {
	ml.Lock()
	defer ml.Unlock()
	ml.org, ml.ip, ml.port, ml.cert, ml.paillierkey = org, ip, port, cert, paillierkey
	log.Debug().Msg("Called NodeConfigurationUpdate")
}

func (ml *mockListener) GuiConfigurationUpdate(ip, cert string) {
	ml.Lock()
	defer ml.Unlock()
	ml.ip, ml.cert = ip, cert
	log.Debug().Msg("Called GuiConfigurationUpdate")
}

// Test if we get node config updates sent by other nodes
func TestNodeConfigurationUpdate(t *testing.T) {
	Setup()
	// Add a node that exists when we start up
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{
		Organization: "TestPartyA",
		Ip:           "111",
		Port:         "115",
		Cert:         "222",
		Paillierkey:  "333",
		Attributes: [][]string{
			{"A", "int"},
			{"B", "string"},
			{"C", "enum", "D", "E", "F"},
		},
		DbHash: "555",
	})
	assert.Nil(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.Nil(t, err, "Can't approve new node")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	ml := &mockListener{}
	c.RegisterNodeConfigListener(ml.NodeConfigurationUpdate)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should have received a node update
	ml.Lock()
	assert.EqualValues(t, "TestPartyA", ml.org)
	assert.EqualValues(t, "111", ml.ip)
	assert.EqualValues(t, "115", ml.port)
	assert.EqualValues(t, "222", ml.cert)
	assert.EqualValues(t, "333", ml.paillierkey)
	ml.Unlock()
	*ml = mockListener{}

	// The contract should give the new database attributes
	logEntry, err := c.GetNodeConfig("TestPartyA")
	assert.Nil(t, err, "Error getting node config after approval")
	assert.EqualValues(t, "TestPartyA", logEntry.Organization)
	assert.EqualValues(t, "111", logEntry.Ip)
	assert.EqualValues(t, "115", logEntry.Port)
	assert.EqualValues(t, "222", logEntry.Cert)
	assert.EqualValues(t, "333", logEntry.Paillierkey)
	assert.EqualValues(t, AttributesFromStrings([][]string{
		{"A", "int"},
		{"B", "string"},
		{"C", "enum", "D", "E", "F"},
	}), logEntry.Attributes)
	assert.EqualValues(t, "555", logEntry.DbHash)

	// Send another node update
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{
		Organization: "TestPartyA",
		Ip:           "101",
		Port:         "105",
		Cert:         "202",
		Paillierkey:  "303",
		Attributes: [][]string{
			{"U", "int"},
			{"V", "string"},
		},
		DbHash: "505",
	})
	assert.Nil(t, err, "Can't register new node again")
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should receive this update too
	ml.Lock()
	assert.EqualValues(t, "TestPartyA", ml.org)
	assert.EqualValues(t, "101", ml.ip)
	assert.EqualValues(t, "105", ml.port)
	assert.EqualValues(t, "202", ml.cert)
	assert.EqualValues(t, "303", ml.paillierkey)
	ml.Unlock()

	// The contract should give the new database attributes
	logEntry, err = c.GetNodeConfig("TestPartyA")
	assert.Nil(t, err, "Error getting node config after approval")
	assert.EqualValues(t, "TestPartyA", logEntry.Organization)
	assert.EqualValues(t, "101", logEntry.Ip)
	assert.EqualValues(t, "105", logEntry.Port)
	assert.EqualValues(t, "202", logEntry.Cert)
	assert.EqualValues(t, "303", logEntry.Paillierkey)
	assert.EqualValues(t, AttributesFromStrings([][]string{
		{"U", "int"},
		{"V", "string"},
	}), logEntry.Attributes)
	assert.EqualValues(t, "505", logEntry.DbHash)
}

// Test if we get GUI config updates sent by the GUI node
func TestGuiConfigurationUpdate(t *testing.T) {
	Setup()
	// Add a node that exists when we start up
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "PartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterGUINode(genesisAccount, sc.PPAStorageGUINodeData{
		Organization: "PartyA",
		Ip:           "111",
		Cert:         "222",
	})
	assert.Nil(t, err, "Can't register new node")
	_, err = presence.ApproveGUINode(genesisAccount, genesisAccount.From)
	assert.Nil(t, err, "Can't approve new node")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	ml := &mockListener{}
	c.RegisterGuiConfigListener(ml.GuiConfigurationUpdate)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should have received a node update
	ml.Lock()
	assert.EqualValues(t, "111", ml.ip)
	assert.EqualValues(t, "222", ml.cert)
	ml.Unlock()
	*ml = mockListener{}

	// The contract should give the new database attributes
	logEntry, err := c.GetGuiConfig("PartyA")
	assert.Nil(t, err, "Error getting node config after approval")
	assert.EqualValues(t, "PartyA", logEntry.Organization)
	assert.EqualValues(t, "111", logEntry.Ip)
	assert.EqualValues(t, "222", logEntry.Cert)

	// Send another node update
	_, err = presence.RegisterGUINode(genesisAccount, sc.PPAStorageGUINodeData{
		Organization: "PartyA",
		Ip:           "101",
		Cert:         "202",
	})
	assert.Nil(t, err, "Can't register new node again")
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should receive this update too
	ml.Lock()
	assert.EqualValues(t, "101", ml.ip)
	assert.EqualValues(t, "202", ml.cert)
	ml.Unlock()

	// The contract should give the new database attributes
	logEntry, err = c.GetGuiConfig("PartyA")
	assert.Nil(t, err, "Error getting node config after approval")
	assert.EqualValues(t, "PartyA", logEntry.Organization)
	assert.EqualValues(t, "101", logEntry.Ip)
	assert.EqualValues(t, "202", logEntry.Cert)
}

// Test if we send node config updates
func TestNodeConfigurationSubmit(t *testing.T) {
	Setup()
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	var firstBlockNr uint64 = 1
	opts := &bind.WatchOpts{Start: &firstBlockNr}

	// Set up the listener for incoming node registration events
	mpcNodeRegisteredChannel := make(chan *sc.PresenceMPCNodeRegistered)
	subscription, _ := presence.WatchMPCNodeRegistered(opts, mpcNodeRegisteredChannel)

	nodeData := sc.PPAStorageMPCNodeData{}
	m := sync.Mutex{}
	go func() {
		for n := range mpcNodeRegisteredChannel {
			m.Lock()
			nodeData = n.NodeData
			m.Unlock()
		}
	}()

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	// Issue a node config update
	assert.Nil(t, c.UpdateNodeConfig(testConfig.Ip, testConfig.Port, testConfig.Cert, testConfig.PaillierKey),
		"Error registering our MPC node with the smart contract")
	defer c.Close()

	_, err := presence.NewAdmin(genesisAccount, "PartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	_, err = presence.ApproveMPCNode(genesisAccount, c.wallet.From)
	assert.Nil(t, err, "Can't approve new node")

	client.Commit()
	time.Sleep(100 * time.Millisecond)

	m.Lock()
	assert.Equal(t, "PartyA", nodeData.Organization)
	assert.Equal(t, "123.45.67.89", nodeData.Ip)
	assert.Equal(t, "443", nodeData.Port)
	assert.Equal(t, "---TLS---", nodeData.Cert)
	assert.Equal(t, "abcd1234", nodeData.DbHash)
	m.Unlock()
	subscription.Unsubscribe()
	close(mpcNodeRegisteredChannel)
}

// Test if we send GUI config updates
func TestGuiConfigurationSubmit(t *testing.T) {
	Setup()
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	var firstBlockNr uint64 = 1
	opts := &bind.WatchOpts{Start: &firstBlockNr}

	// Set up the listener for incoming node registration events
	guiNodeRegisteredChannel := make(chan *sc.PresenceGUINodeRegistered)
	subscription, _ := presence.WatchGUINodeRegistered(opts, guiNodeRegisteredChannel)

	nodeData := sc.PPAStorageGUINodeData{}
	m := sync.Mutex{}
	go func() {
		for n := range guiNodeRegisteredChannel {
			m.Lock()
			nodeData = n.NodeData
			m.Unlock()
		}
	}()

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	// Issue a node config update
	assert.Nil(t, c.UpdateGuiConfig(testConfig.Ip, testConfig.Cert),
		"Error registering our GUI node with the smart contract")
	defer c.Close()

	_, err := presence.NewAdmin(genesisAccount, "PartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	_, err = presence.ApproveGUINode(genesisAccount, c.wallet.From)
	assert.Nil(t, err, "Can't approve new node")

	client.Commit()
	time.Sleep(100 * time.Millisecond)

	m.Lock()
	assert.Equal(t, "PartyA", nodeData.Organization)
	assert.Equal(t, "123.45.67.89", nodeData.Ip)
	assert.Equal(t, "---TLS---", nodeData.Cert)
	m.Unlock()
	subscription.Unsubscribe()
	close(guiNodeRegisteredChannel)
}

// Test if we get new query notifications
func TestNewQuery(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.Nil(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.Nil(t, err, "Can't approve new node")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	ml := &mockListener{}
	c.RegisterQueryListener(ml.IncomingQuery)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()

	// Submit a query
	scq, _ := sc.NewQuery(common.HexToAddress(testConfig.EthereumContractAddress), client)
	query := sc.PPAQueryQuery{
		Columns: []sc.PPAQueryColumn{{
			Func:      "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Constraints: []sc.PPAQueryConstraint{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := sc.PPAQueryUserMetadata{
		Groupkey:         "group_twelve",
		DestinationNodes: []string{"N", "M"},
	}

	scq.SubmitQuery(genesisAccount, query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should have received a node update
	ml.Lock()
	assert.NotEqual(t, "", ml.queryid)
	assert.Equal(t, "group_twelve", ml.options["groupkey"])
	assert.Equal(t, []string{"N", "M"}, ml.options["destinationNodes"])
	assert.Equal(t, PPAQueryQuery(query).TypesQuery(), ml.query)
	ml.Unlock()
}

// Test if metadata from constraints is propagated correctly
func TestNewQueryMetadata(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.Nil(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.Nil(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.Nil(t, err, "Can't approve new node")

	// Add some rules for queries
	maintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)

	_, err = maintenance.AddStdevConstraint(genesisAccount, sc.PPAQueryMaintenanceStdevConstraint{
		Owner:     "P",
		Attribute: "Q",
		MinStdev:  big.NewInt(123456789),
	})
	assert.Nil(t, err, "Can't add stdev constraint")
	_, err = maintenance.AddPercentageConstraint(genesisAccount, sc.PPAQueryMaintenancePercentageConstraint{
		Owner:         "P",
		Attribute:     "Q",
		MinPercentage: big.NewInt(4),
		MaxPercentage: big.NewInt(80),
	})
	assert.Nil(t, err, "Can't add percentage constraint")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	ml := &mockListener{}
	c.RegisterQueryListener(ml.IncomingQuery)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()

	// Submit a query
	scq, _ := sc.NewQuery(common.HexToAddress(testConfig.EthereumContractAddress), client)
	query := sc.PPAQueryQuery{
		Columns: []sc.PPAQueryColumn{{
			Func:      "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Constraints: []sc.PPAQueryConstraint{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := sc.PPAQueryUserMetadata{}

	scq.SubmitQuery(genesisAccount, query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// We should have received a node update
	ml.Lock()
	assert.NotEqual(t, "", ml.queryid)
	// Comparing floats is a bit cumbersome
	assert.Equal(t, PPAQueryQuery(query).TypesQuery(), ml.query)
	assert.Equal(t, 1, len(ml.options["stdevConstraints"].([]coordinator.StdevConstraint)))
	assert.Equal(t, "P", ml.options["stdevConstraints"].([]coordinator.StdevConstraint)[0].Owner)
	assert.Equal(t, "Q", ml.options["stdevConstraints"].([]coordinator.StdevConstraint)[0].Attribute)
	msd, _, _ := big.ParseFloat("3.6280689510039781342e-31", 10, 64, 0)
	msd.Abs(msd.Sub(msd, ml.options["stdevConstraints"].([]coordinator.StdevConstraint)[0].MinStdev))
	assert.Equal(t, -1, msd.Cmp(big.NewFloat(0.000000001)))

	assert.Equal(t, []coordinator.PercentageConstraint{{
		Owner:         "P",
		Attribute:     "Q",
		MinPercentage: big.NewInt(4),
		MaxPercentage: big.NewInt(80),
	}}, ml.options["percentageConstraints"])
	ml.Unlock()
}

// Test if we correctly submit results
func TestSubmitResult(t *testing.T) {
	Setup()
	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	ml := &mockListener{}
	c.RegisterQueryListener(ml.IncomingQuery)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()

	// Set up the result writer
	logger := c.Writer()

	// Set up a result listener
	scq, _ := sc.NewQuery(common.HexToAddress(testConfig.EthereumContractAddress), client)
	var firstBlockNr uint64 = 1
	opts := &bind.WatchOpts{Start: &firstBlockNr}
	resultChannel := make(chan *sc.QueryQueryComplete)
	subscription, _ := scq.WatchQueryComplete(opts, resultChannel)
	submittedResult := new(sc.QueryQueryComplete)

	m := sync.Mutex{}
	go func() {
		for res := range resultChannel {
			m.Lock()
			*submittedResult = *res
			m.Unlock()
		}
	}()

	// Submit a result
	qid1, _ := hex.DecodeString("c5059fe45f454844b2d0106d7d790e411338dcbf6d957579d7bdb8a551e41a69")
	goodResult, _ := json.Marshal(map[string]string{
		"queryid":    "c5059fe45f454844b2d0106d7d790e411338dcbf6d957579d7bdb8a551e41a69",
		"resulthash": "abcdef",
	})
	logger.Write(goodResult)
	// Allow time for log listener goroutine and funds top-up (2sec)
	time.Sleep(3 * time.Second)
	client.Commit()                    // Mine blockchain transaction
	time.Sleep(100 * time.Millisecond) // Allow time for blockchain listener goroutine

	// Check if it's there
	m.Lock()
	assert.Equal(t, qid1, submittedResult.QueryID[:], "Query ID not correct (1)")
	assert.Equal(t, "abcdef", submittedResult.ResultHash, "Result hash not correct (1)")
	assert.NotEqual(t, common.Address{}, submittedResult.From, "Address is zero (1)")
	m.Unlock()

	// Submit another result
	qid2, _ := hex.DecodeString("837ed992bfe3d49292abf9aaeb1fa2f58eccf55e2ef6d4638633d0c99d281bf0")
	abortResult, _ := json.Marshal(map[string]string{
		"queryid":    "837ed992bfe3d49292abf9aaeb1fa2f58eccf55e2ef6d4638633d0c99d281bf0",
		"resulthash": "aborted",
	})
	logger.Write(abortResult)
	// Allow time for log listener goroutine and funds top-up (2sec)
	time.Sleep(3 * time.Second)
	client.Commit()                    // Mine blockchain transaction
	time.Sleep(100 * time.Millisecond) // Allow time for blockchain listener goroutine

	// Check if it's there
	m.Lock()
	assert.Equal(t, qid2, submittedResult.QueryID[:], "Query ID not correct (2)")
	assert.Equal(t, "aborted", submittedResult.ResultHash, "Result hash not correct (2)")
	assert.NotEqual(t, common.Address{}, submittedResult.From, "Address is zero (2)")
	m.Unlock()

	subscription.Unsubscribe()
}

func TestCloseConnection(t *testing.T) {
	Setup()
	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	assert.Equal(t, Disconnected, c.Status)

	c.SetClient(client)
	assert.Equal(t, Disconnected, c.Status)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	assert.Equal(t, Connected, c.Status)

	c.Close()
	assert.Equal(t, Disconnected, c.Status)

	// Allow for other goroutines to get the signal
	time.Sleep(100 * time.Millisecond)

	assert.Equal(t, Disconnected, c.Status)

	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	assert.Equal(t, Connected, c.Status)

	c.Close()
	assert.Equal(t, Disconnected, c.Status)
}
