// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package coordinator

import (
	"math/big"
)

// NewQueryMetadata is the type for metadata that can be chosen by the submitter
// of a new query.
type NewQueryMetadata struct {
	Groupkey         string   `json:"groupkey"`
	DestinationNodes []string `json:"destination_nodes"`
}

// QueryMetadata is a collection of metadata entries corresponding to an approved
// query, and is returned as part of a Query Approved event from the smartcontract.
type QueryMetadata struct {
	MinResultSize         *big.Int `swaggertype:"integer" example:"45"`
	MinResultPercentage   *big.Int `swaggertype:"integer" example:"1"`
	MaxResultPercentage   *big.Int `swaggertype:"integer" example:"75"`
	PercentageConstraints []PercentageConstraint
	StdevConstraints      []StdevConstraint
	Groupkey              string
	DestinationNodes      []string
	Reviewer              string
}

// A PercentageConstraint is a per-column version of Min/MaxResultPercentage
type PercentageConstraint struct {
	Owner         string   `example:"ACME"`
	Attribute     string   `example:"attribute1"`
	MinPercentage *big.Int `example:"2" swaggertype:"integer"`
	MaxPercentage *big.Int `example:"85" swaggertype:"integer"`
}

// A StdevConstraint expresses the requirement that an aggregate describing a filtered
// subset of values of a column (Owner).(Attribute) is computed over a population for
// which the attribute values have a minimum standard deviation of (MinStdev).
type StdevConstraint struct {
	Owner     string     `example:"ACME"`
	Attribute string     `example:"attribute1"`
	MinStdev  *big.Float `swaggertype:"number" example:"0.100000000000000005551115123125782702118"`
}
