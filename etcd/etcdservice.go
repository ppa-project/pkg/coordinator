// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package etcd

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.etcd.io/etcd/clientv3"
)

func (c *Coordinator) getMPCNodes() (*clientv3.GetResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	resp, err := c.cli.Get(ctx, "/mpc_node", clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortAscend))
	cancel()
	if err != nil {
		return nil, &EtcdError{Err: err}
	}
	return resp, nil
}

func (c *Coordinator) getMPCNodeConfig(organization string) (org string, ip string, port string, cert string, paillierkey string, reterr error) {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	resp, err := c.cli.Get(ctx, fmt.Sprintf("/mpc_node/%s", organization), clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortAscend))
	cancel()
	if err != nil {
		reterr = &EtcdError{err, "Get failed"}
		return
	}
	for _, ev := range resp.Kvs {
		c.log.Debug().Msgf("%s : %s", ev.Key, ev.Value)
		varName, ok := getPathIndex(ev.Key, 2)
		if !ok {
			continue
		}
		switch varName {
		case "org":
			org = string(ev.Value)
		case "ip":
			ip = string(ev.Value)
		case "port":
			port = string(ev.Value)
		case "cert":
			cert = string(ev.Value)
		case "paillierkey":
			paillierkey = string(ev.Value)
		}
	}
	if org == "" || ip == "" || cert == "" || paillierkey == "" {
		reterr = errors.New("Node Configuration incomplete")
	}
	return
}

func (c *Coordinator) getGUIConfig() (ip string, cert string, reterr error) {
	ctx, cancel := context.WithTimeout(context.Background(), etcdDialTimeout)
	resp, err := c.cli.Get(ctx, fmt.Sprintf("/gui_node/%s", c.config.Organization), clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortAscend))
	cancel()
	if err != nil {
		reterr = &EtcdError{err, "Get failed"}
		return
	}
	if len(resp.Kvs) == 0 {
		reterr = errors.New("No GUI configuration found")
		return
	}
	for _, ev := range resp.Kvs {
		c.log.Debug().Msgf("%s : %s", ev.Key, ev.Value)
		varName, ok := getPathIndex(ev.Key, 2)
		if !ok {
			continue
		}
		switch varName {
		case "ip":
			ip = string(ev.Value)
		case "cert":
			cert = string(ev.Value)
		}
	}
	if ip == "" || cert == "" {
		reterr = errors.New("GUI Configuration incomplete")
	}
	return
}

func (c *Coordinator) setMPCNodeConfig(ip, port, cert, paillierkey string) error {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	prefix := fmt.Sprintf("/mpc_node/%s/", c.config.Organization)
	attributesJSON, _ := c.config.DatabaseAttributes.MarshalJSON()
	_, err := c.cli.Txn(ctx).
		Then(clientv3.OpPut(prefix+"org", c.config.Organization),
			clientv3.OpPut(prefix+"ip", ip),
			clientv3.OpPut(prefix+"port", port),
			clientv3.OpPut(prefix+"cert", cert),
			clientv3.OpPut(prefix+"paillierkey", paillierkey),
			clientv3.OpPut(prefix+"attributes", string(attributesJSON))).
		Commit()
	cancel()
	if err != nil {
		return &EtcdError{err, "setMPCNodeConfig failed"}
	}
	return nil
}

func (c *Coordinator) setGUINodeConfig(ip, cert string) error {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	prefix := fmt.Sprintf("/gui_node/%s/", c.config.Organization)
	_, err := c.cli.Txn(ctx).
		Then(clientv3.OpPut(prefix+"ip", ip),
			clientv3.OpPut(prefix+"cert", cert)).
		Commit()
	cancel()
	if err != nil {
		return &EtcdError{err, "setGUINodeConfig failed"}
	}
	return nil
}

func (c *Coordinator) setQuery(protocol, id, query string) error {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	key := fmt.Sprintf("/query/%s/%s", protocol, id)
	_, err := c.cli.Txn(ctx).
		Then(clientv3.OpPut(key, query)).
		Commit()
	cancel()
	if err != nil {
		return &EtcdError{err, "setQuery failed"}
	}
	return nil
}

func (c *Coordinator) setHealth(nodeName, message string) error {
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	key := fmt.Sprintf("/health/%s/%s", c.config.Organization, nodeName)
	currentTime := time.Now()
	_, err := c.cli.Txn(ctx).
		Then(clientv3.OpPut(key, message),
			clientv3.OpPut(key+"/time", currentTime.String())).
		Commit()
	cancel()
	if err != nil {
		return &EtcdError{err, "setHealth failed"}
	}
	return nil
}
