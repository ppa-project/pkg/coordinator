// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/ethereum/go-ethereum/common"
)

// EventSubmitTimeout is the timeout for result and review event log submissions
var EventSubmitTimeout = 20 * time.Second

// Listens to the log channel and submits logs to the smart contract
func (scc *Coordinator) logLoop() {
	for logEntry := range scc.logsIn {
		scc.Lock()

		if scc.Status == Connected {
			queryid := logEntry.Get("queryid").(string)
			qidBytes := [32]byte{}
			copy(qidBytes[:], common.HexToHash(queryid).Bytes())

			var err error
			if logEntry.Contains("reviewer") {
				err = scc.submitReviewEvent(qidBytes, logEntry.Get("reviewer").(string))
			} else {
				// A query is either aborted or logs a 32-byte hash of the result
				err = scc.submitResultEvent(qidBytes, logEntry.GetOr("resulthash", "aborted").(string))
			}
			if err != nil {
				scc.Unlock()
				scc.connectionError(err)
				scc.Lock()
				scc.logsBackup = append(scc.logsBackup, logEntry)
			}
		} else {
			// Here, we're disconnected or reconnecting
			// Save this log and keep listening
			scc.logsBackup = append(scc.logsBackup, logEntry)
		}

		scc.Unlock()
	}
}

// Submit logs collected while disconnected, and submit them
func (scc *Coordinator) submitBackedUpLogs() error {
	scc.Lock()
	defer scc.Unlock()
	for len(scc.logsBackup) != 0 {
		queryid := scc.logsBackup[0].Get("queryid").(string)
		qidBytes := [32]byte{}
		copy(qidBytes[:], common.HexToHash(queryid).Bytes())

		var err error
		if scc.logsBackup[0].Contains("reviewer") {
			err = scc.submitReviewEvent(qidBytes, scc.logsBackup[0].Get("reviewer").(string))
		} else {
			// A query is either aborted or logs a 32-byte hash of the result
			err = scc.submitResultEvent(qidBytes, scc.logsBackup[0].GetOr("resulthash", "aborted").(string))
		}
		if err != nil {
			return err
		}
		scc.logsBackup = scc.logsBackup[1:]
	}
	return nil
}

// Actually send the reviewevent to the smart contract. Requires lock.
func (scc *Coordinator) submitReviewEvent(queryid [32]byte, reviewer string) error {
	ctx, cancel := context.WithTimeout(context.Background(), EventSubmitTimeout)
	defer cancel()

	if err := scc.ensureFunds(ctx); err != nil {
		log.Err(err).
			Msg("Can't submit review event to the smart contract; insufficient funds and can't top up")
		return err
	}

	tx, err := scc.smartContract.SubmitManualReview(scc.wallet, queryid, reviewer)
	if err != nil {
		log.Err(err).
			Str("transaction", tx.Hash().Hex()).
			Msg("Submission of query review event failed")
		return err
	}

	err = scc.awaitTransaction(ctx, tx.Hash())
	// Log our operation. Never add parameters to this log that could cause it to be
	// filtered by CoordinatorWriter.Write, as that would create a loop!
	log.Err(err).
		Str("transaction", tx.Hash().Hex()).
		Msg("Submitted query review event to smart contract")
	return err
}

// Actually send the result event to the smart contract. Requires lock.
func (scc *Coordinator) submitResultEvent(queryid [32]byte, resulthash string) error {
	ctx, cancel := context.WithTimeout(context.Background(), EventSubmitTimeout)
	defer cancel()

	if err := scc.ensureFunds(ctx); err != nil {
		log.Err(err).
			Msg("Can't submit result to the smart contract; insufficient funds and can't top up")
		return err
	}

	tx, err := scc.smartContract.SubmitResult(scc.wallet, queryid, resulthash)
	if err != nil {
		log.Err(err).
			Str("transaction", tx.Hash().Hex()).
			Msg("Submission of query result failed")
		return err
	}

	err = scc.awaitTransaction(ctx, tx.Hash())
	// Log our operation. Never add parameters to this log that could cause it to be
	// filtered by CoordinatorWriter.Write, as that would create a loop!
	log.Err(err).
		Str("transaction", tx.Hash().Hex()).
		Msg("Submitted query result to smart contract")
	return err
}
