// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test if accepted queries are reported correctly
func TestDPaillierMutex(t *testing.T) {
	assert := assert.New(t)
	Setup()

	c := NewCoordinator(*testConfig)
	c.SetClient(client)
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	c.wallet = genesisAccount
	client.Commit()

	// Mutex should be open and version should be empty
	version, holder, err := c.DPaillierRead(context.Background())
	assert.NoError(err)
	assert.Equal("", version)
	assert.Equal("0x0000000000000000000000000000000000000000", holder.Hex())

	// Lock the mutex
	err = c.DPaillierLock(context.Background())
	assert.NoError(err)

	// Mutex should be closed and version should be empty
	version, holder, err = c.DPaillierRead(context.Background())
	assert.NoError(err)
	assert.Equal("", version)
	assert.Equal(genesisAccount.From, holder)

	// Lock the mutex again!
	err = c.DPaillierLock(context.Background())
	if assert.Error(err) {
		assert.Equal("failed to estimate gas needed: execution reverted: Mutex is closed, can not be taken", err.Error())
	}

	// Mutex should be closed and version should be empty
	version, holder, err = c.DPaillierRead(context.Background())
	assert.NoError(err)
	assert.Equal("", version)
	assert.Equal(genesisAccount.From, holder)

	// Unlock the mutex
	err = c.DPaillierUnlock(context.Background(), "v1.1")
	assert.NoError(err)

	// Mutex should be open and version should be 1.1
	version, holder, err = c.DPaillierRead(context.Background())
	assert.NoError(err)
	assert.Equal("v1.1", version)
	assert.Equal("0x0000000000000000000000000000000000000000", holder.Hex())

	// Unlock the mutex again!
	err = c.DPaillierUnlock(context.Background(), "v1.2")
	if assert.Error(err) {
		assert.Equal("failed to estimate gas needed: execution reverted: Mutex is open, can not be released twice", err.Error())
	}

	// Mutex should be open and version should be 1.1
	version, holder, err = c.DPaillierRead(context.Background())
	assert.NoError(err)
	assert.Equal("v1.1", version)
	assert.Equal("0x0000000000000000000000000000000000000000", holder.Hex())
}
