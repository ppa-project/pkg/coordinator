// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package coordinator

import (
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// Config is the data we need to pass to the smart contract to register ourselves,
// plus some additional connection parameters.
type Config struct {
	Organization            string
	Ip                      string
	Port                    string
	Cert                    string
	PaillierKey             string
	Attributes              types.DatabaseAttributes
	DbHash                  string
	EthereumPrivateKey      string
	EthereumURL             string
	EthereumContractAddress string
}
