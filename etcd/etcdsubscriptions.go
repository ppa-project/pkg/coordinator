// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package etcd

import (
	"context"
	"errors"
	"strings"
	"time"

	"go.etcd.io/etcd/clientv3"
)

func (c *Coordinator) subscribeEvents() error {
	ctx, cancel := context.WithCancel(context.Background())

	c.log.Debug().Msg("Connecting watchers to cluster...")
	t := time.NewTimer(3 * etcdGetTimeout)
	done := make(chan bool)

	go func() {
		select {
		case <-t.C:
			c.log.Debug().Msg("Connect watchers timeout")
			cancel()
		case <-done:
		}
	}()

	mpcNodeChannel := c.cli.Watch(ctx, "/mpc_node", clientv3.WithPrefix())
	guiNodeChannel := c.cli.Watch(ctx, "/gui_node", clientv3.WithPrefix())
	queryChannel := c.cli.Watch(ctx, "/query", clientv3.WithPrefix())

	close(done)

	if ctx.Err() != context.Canceled {
		c.log.Debug().Msg("Watchers connected to cluster")
		go func() {
			for {
				select {
				case newNodeEvent := <-mpcNodeChannel:
					go c.dispatchNewNodeConfigWithWatchResponse(newNodeEvent)
				case newGuiEvent := <-guiNodeChannel:
					go c.dispatchNewGuiConfigWithWatchResponse(newGuiEvent)
				case newQueryEvent := <-queryChannel:
					go c.dispatchNewQuery(newQueryEvent)
				}
			}
		}()
	} else {
		return errors.New("Cannot connect watchers to cluster")
	}

	return nil
}

// When starting up, we need each node's configuration immediately.
func (c *Coordinator) dispatchMostRecentNodeConfigEvents() error {
	nodes, err := c.getMPCNodes()
	if err != nil {
		return err
	}

	set := make(map[string]bool)
	for _, kv := range nodes.Kvs {
		orgName, ok := getPathIndex(kv.Key, 1)
		if ok {
			set[orgName] = true
		}
	}
	// Call the node presence listeners once for each org
	for org := range set {
		c.dispatchNewNodeConfig(org)
	}

	return nil
}

// When starting up, we need our GUI's configuration immediately.
func (c *Coordinator) dispatchMostRecentGuiConfigEvents() error {
	c.dispatchNewGuiConfig()
	return nil
}

func (c *Coordinator) dispatchNewNodeConfig(organization string) {
	c.Lock()
	defer c.Unlock()

	org, ip, port, cert, paillierkey, err := c.getMPCNodeConfig(organization)
	if err != nil {
		c.log.Error().Err(err).Str("organizatation", organization).Msg("Failed to get configuration for organization")
	}
	for i := range c.presenceListeners {
		c.presenceListeners[i](
			org,
			ip,
			port,
			cert,
			paillierkey,
		)
	}
}

func (c *Coordinator) dispatchNewNodeConfigWithWatchResponse(newNodeEvent clientv3.WatchResponse) {
	set := make(map[string]bool)
	for _, ev := range newNodeEvent.Events {
		orgName, ok := getPathIndex(ev.Kv.Key, 1)
		if ok {
			set[orgName] = true
		}
	}
	for org := range set {
		c.dispatchNewNodeConfig(org)
	}
}

func (c *Coordinator) dispatchNewGuiConfig() {
	c.Lock()
	defer c.Unlock()

	ip, cert, err := c.getGUIConfig()
	if err != nil {
		c.log.Warn().Err(err).Msg("Failed to get own GUI configuration")
		return
	}

	for i := range c.guiListeners {
		c.guiListeners[i](
			ip,
			cert,
		)
	}
}

func (c *Coordinator) dispatchNewGuiConfigWithWatchResponse(newGuiEvent clientv3.WatchResponse) {
	if len(newGuiEvent.Events) == 0 {
		return
	}
	orgName, ok := getPathIndex(newGuiEvent.Events[0].Kv.Key, 1)
	if !ok {
		return
	}
	if orgName != c.config.Organization {
		return
	}
	c.dispatchNewGuiConfig()
}

func (c *Coordinator) dispatchNewQuery(newQueryEvent clientv3.WatchResponse) {
	c.Lock()
	defer c.Unlock()
	for _, ev := range newQueryEvent.Events {
		qType, ok := getPathIndex(ev.Kv.Key, 1)
		if !ok {
			return
		}
		qid, ok := getPathIndex(ev.Kv.Key, 2)
		if !ok {
			return
		}
		for i := range c.queryListeners {
			c.queryListeners[i](
				qType,
				qid,
				ev.Kv.Value,
				nil,
			)
		}
	}
}

func getPathIndex(pathBytes []byte, idx int) (string, bool) {
	split := strings.Split(string(pathBytes), "/")[1:]
	if len(split) < idx+1 {
		return "", false
	}
	return split[idx], true
}
