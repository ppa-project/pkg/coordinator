// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package coordinator

// QueryDetails is the collection of log entries about a single query that has been approved.
// LogEntryQueryApproved must not be nil if the corresponding query ID exists, but the other two may be.
type QueryDetails struct {
	*LogEntryQueryApproved
	*LogEntryQueryManualReview
	*LogEntryQueryComplete
}
