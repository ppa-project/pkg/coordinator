// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

type PPAQueryStdevConstraint sc.PPAQueryStdevConstraint

// StdevConstraint converts the smart contract representation of a standard deviation
// constraint to a representation suitable for use with the basicstats protocol.
func (scc PPAQueryStdevConstraint) StdevConstraint() coordinator.StdevConstraint {
	// From the documentation in PPAQuery.sol:
	// The minimum standard deviation is stored as integer, since there are no fractional
	// numbers (in Solidity -ed). Should be interpreted as a fixed point number with 128
	// fractional bits - that is, real_minStdev = minStdev * 2**(-128).

	// SetMantExp sets z to mant × 2**exp and returns z.
	stDevAsFloat := new(big.Float).SetInt(scc.MinStdev)
	stDevAsFloat.SetMantExp(stDevAsFloat, -128)

	return coordinator.StdevConstraint{
		Owner:     scc.Owner,
		Attribute: scc.Attribute,
		MinStdev:  stDevAsFloat,
	}
}
