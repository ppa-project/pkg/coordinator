// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// NewMPCRegistrationData converts the given config to the type required by the smart contract
func NewMPCRegistrationData(config coordinator.Config) sc.PPAStorageMPCNodeData {
	return sc.PPAStorageMPCNodeData{
		Organization: config.Organization,
		Ip:           config.Ip,
		Port:         config.Port,
		Cert:         config.Cert,
		Paillierkey:  config.PaillierKey,
		Attributes:   AttributesIntoStrings(config.Attributes),
		DbHash:       config.DbHash,
	}
}

// NewGUIRegistrationData converts the given config to the type required by the smart contract
func NewGUIRegistrationData(config coordinator.Config) sc.PPAStorageGUINodeData {
	return sc.PPAStorageGUINodeData{
		Organization: config.Organization,
		Ip:           config.Ip,
		Cert:         config.Cert,
	}
}

// The smartcontract can not take nested arrays of structs, so we convert
// the attributes slice to a string double slice and back at the boundary.
// AttributesIntoStrings takes a slice of DatabaseAttributes and produces strings.
func AttributesIntoStrings(attributes types.DatabaseAttributes) [][]string {
	convertedAttributes := make([][]string, len(attributes))
	for i := range []types.DatabaseAttribute(attributes) {
		convertedAttributes[i] = make([]string, 2, 2+len(attributes[i].EnumValues))
		convertedAttributes[i][0] = attributes[i].Name
		convertedAttributes[i][1] = attributes[i].Kind
		convertedAttributes[i] = append(convertedAttributes[i], attributes[i].EnumValues...)
	}
	return convertedAttributes
}

// The smartcontract can not take nested arrays of structs, so we convert
// the attributes slice to a string double slice and back at the boundary.
// AttributesIntoStrings takes a slice of DatabaseAttributes and produces strings.
func AttributesFromStrings(encodedAttributes [][]string) types.DatabaseAttributes {
	attributes := make([]types.DatabaseAttribute, len(encodedAttributes))
	for i := range encodedAttributes {
		attributes[i].Name = encodedAttributes[i][0]
		attributes[i].Kind = encodedAttributes[i][1]
		if attributes[i].Kind == "enum" && len(encodedAttributes[i]) > 2 {
			attributes[i].EnumValues = encodedAttributes[i][2:]
		}
	}
	return types.DatabaseAttributes(attributes)
}
