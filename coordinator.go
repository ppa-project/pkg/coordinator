// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package coordinator provides the coordinator interface, which describes the configuration
// synchronization service common to the PPA nodes.
package coordinator

import (
	"context"
	"io"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"

	"github.com/ethereum/go-ethereum/common"
)

// The Coordinator acts as a hub for configuration of all MPC and GUI nodes,
// and as the service that broadcasts new queries to MPC nodes and collects
// result hashes.
//
// Normally backed by the Coordinator, but for testing purposes,
// an etcd-based backend can be used as a coordinator instead.
type Coordinator interface {
	RegisterQueryListener(ql QueryListener)
	RegisterNodeConfigListener(nl NodeConfigListener)
	RegisterGuiConfigListener(nl GuiConfigListener)
	RegisterAuditLogEntryListener(alel AuditLogEntryListener)
	Writer() io.Writer

	ConnectAndListen() error
	Close()

	UpdateNodeConfig(ip, port, cert, paillierkey string) error
	UpdateGuiConfig(ip, cert string) error
	UpdateHealth(nodeName string, healthy bool, err error) error

	GetOrganizations() ([]string, error)
	GetNodeConfig(organization string) (*LogEntryPresenceMPC, error)
	GetGuiConfig(organization string) (*LogEntryPresenceGUI, error)

	AuditLogLen(filter LogKind) int
	AuditLog(filter LogKind, start, count int) []LogEntry

	SubmitNewQuery(newQuery types.Query, metadata NewQueryMetadata) (string, error)
	GetQueryDetails(queryid string) (QueryDetails, error)

	GetBusinessRules() []string

	DPaillierLock(context.Context) error
	DPaillierUnlock(context.Context, string) error
	DPaillierRead(context.Context) (string, common.Address, error)
}

// The coordinator produces newly approved queries. Any entity wishing to subscribe
// to these events should supply a function that conforms to QueryListener.
// When a new query arrives through the coordinator, this function is
// called (exactly once) for each registered QueryListener.
type QueryListener func(protocolName, queryid string, input interface{}, options map[string]interface{})

// The smart contract broadcasts changes to node configuration. Any entity wishing to
// subscribe to these events should supply a function that conforms to NodeConfigListener.
// When a a notification containing new configuration arrives through the smart
// contract, NodeConfigurationUpdate is called (exactly once) for each registered
// NodeConfigListener.
// `org` is an organization identifier, `ip` is the MPC node's IP address,
// `cert` is the node's TLS certificate for use in HTTPS connections,
// and `paillierkey` is the node's Paillier public key (as json).
type NodeConfigListener func(org, ip, port, cert, paillierkey string)

// The smart contract broadcasts changes to GUI configuration. Any entity wishing to
// subscribe to these events should supply a function that conforms to GuiConfigListener.
// When a a notification containing new configuration arrives through the smart
// contract, GuiConfigurationUpdate is called (exactly once) for each registered
// GuiConfigListener.
// Only changes for the current organization are reported.
// ip` is the MPC Gui's IP address, and `cert` is the Gui's TLS certificate for
// use in HTTPS connections.
type GuiConfigListener func(ip, cert string)

// The smart contract broadcasts new auditlog events. Any entity wishing to
// subscribe to these events should supply a function that conforms to AuditLogEntryListener.
type AuditLogEntryListener func(entry LogEntry)
