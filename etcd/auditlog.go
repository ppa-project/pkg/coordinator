// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package etcd

import (
	"errors"
	"github.com/rs/zerolog/log"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
)

// AuditLogLen reports how many audit logs exist for the specified filter.
// If the logbook is not (yet) initialized, this function returns 0.
func (c *Coordinator) AuditLogLen(filter coordinator.LogKind) int {
	log.Error().Msg("Unimplemented: AuditLogLen")
	return 0
}

// AuditLogs returns audit logs from the Coordinator's cache matching the given
// filter. The first (start) entries are skipped, and a maximum of (count) entries
// are returned. If count is zero, there is no maximum.
// If the logbook is not (yet) initialized, this function returns a nil slice.
func (c *Coordinator) AuditLog(filter coordinator.LogKind, start, count int) []coordinator.LogEntry {
	log.Error().Msg("Unimplemented: AuditLog")
	return nil
}

// GetQueryDetails returns any log entries corresponding to the given query ID.
// If no query is found that corresponds to the given ID, and an error is returned.
// Otherwise, the first return value contains the approval event, and the second
// value may be nil or contain the completion event.
func (c *Coordinator) GetQueryDetails(queryid string) (coordinator.QueryDetails, error) {
	log.Error().Msg("Unimplemented: GetQueryDetails")
	return coordinator.QueryDetails{}, errors.New("Unimplemented")
}
