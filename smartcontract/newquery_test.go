// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"math/big"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// Test if accepted queries are reported correctly
func TestNewQuerySubmitAccepted(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.NoError(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.NoError(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.NoError(t, err, "Can't approve new node")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	c.wallet = genesisAccount
	client.Commit()

	// Submit a query
	query := types.Query{
		Aggregates: []types.Aggregate{{
			Function:  "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Filters: []types.Filter{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := coordinator.NewQueryMetadata{}

	queryid, err := c.SubmitNewQuery(query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	assert.NoError(t, err, "SubmitQuery gave an error")
	assert.Equal(t, 64, len(queryid), "Queryid seems wrong, it was %s", queryid)
}

// Test if query rule errors are reported correctly
func TestNewQuerySubmitRejected(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.NoError(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.NoError(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.NoError(t, err, "Can't approve new node")

	// Add a rule we will violate
	maintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)

	_, err = maintenance.AddRule(genesisAccount, sc.PPAQueryMaintenanceRuleData{
		Kind:   big.NewInt(1), // Parameter can not occur in the filter
		Params: [8]string{"A", "a"},
	})
	assert.NoError(t, err, "Can't add query rule")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	c.wallet = genesisAccount
	client.Commit()

	// Submit a query
	query := types.Query{
		Aggregates: []types.Aggregate{{
			Function:  "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Filters: []types.Filter{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := coordinator.NewQueryMetadata{}

	queryid, err := c.SubmitNewQuery(query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	if assert.Error(t, err, "SubmitQuery gave no error") {
		assert.Equal(t, "query rejected by the smart contract for violation of rule 0", err.Error(), "SubmitQuery gave the wrong error")
	}
	assert.Equal(t, "", queryid, "Queryid should be empty")
}

// Test if rate limit errors are reported correctly
// The simulated backend is not very lifelike when it comes to block times.
// Therefore, we can only test a rate limit of 0 queries.
func TestNewQuerySubmitRateLimited(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.NoError(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.NoError(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.NoError(t, err, "Can't approve new node")

	// Add a rate limit we will violate
	maintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)

	_, err = maintenance.AddTimeConstraint(genesisAccount, sc.PPAQueryMaintenanceTimeConstraint{
		MaxQueries: big.NewInt(0),
		Timespan:   big.NewInt(10000000),
	})
	assert.NoError(t, err, "Can't add time constraint")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	c.wallet = genesisAccount
	client.Commit()

	// Submit a query
	query := types.Query{
		Aggregates: []types.Aggregate{{
			Function:  "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Filters: []types.Filter{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := coordinator.NewQueryMetadata{}

	queryid, err := c.SubmitNewQuery(query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	if assert.Error(t, err, "SubmitQuery gave no error") {
		assert.Equal(t, "query rejected by the smart contract due to the rate limit: you previously submitted queries a", err.Error(), "SubmitQuery gave the wrong error")
	}
	assert.Equal(t, "", queryid, "Queryid should be empty")
}

// Test if query budget errors are reported correctly
func TestNewQuerySubmitUnaffordable(t *testing.T) {
	Setup()
	// Set up an account in the smart contract from which to submit the query
	presence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := presence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.NoError(t, err, "Can't add new admin to presence")
	client.Commit()
	_, err = presence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{Organization: "TestPartyA"})
	assert.NoError(t, err, "Can't register new node")
	_, err = presence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.NoError(t, err, "Can't approve new node")

	// Add a rate limit we will violate
	maintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)

	_, err = maintenance.SetPrivacyBudget(genesisAccount, "TestPartyA", big.NewInt(1))
	assert.NoError(t, err, "Can't set privacy budget")
	_, err = maintenance.SetColumnCost(genesisAccount, "A", "a", big.NewInt(10000))
	assert.NoError(t, err, "Can't set column cost")

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	c.wallet = genesisAccount
	client.Commit()

	// Submit a query
	query := types.Query{
		Aggregates: []types.Aggregate{{
			Function:  "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Filters: []types.Filter{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	metadata := coordinator.NewQueryMetadata{}

	queryid, err := c.SubmitNewQuery(query, metadata)
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	if assert.Error(t, err, "SubmitQuery gave no error") {
		assert.Equal(t, "query rejected by the smart contract for exceeding the query budget: it costs 10000 and you have only 1", err.Error(), "SubmitQuery gave the wrong error")
	}
	assert.Equal(t, "", queryid, "Queryid should be empty")
}
