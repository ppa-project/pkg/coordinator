// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package etcd

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"sync"
	"time"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"

	"github.com/ethereum/go-ethereum/common"
	"github.com/rs/zerolog"
	l "github.com/rs/zerolog/log"
	"go.etcd.io/etcd/clientv3"
)

const (
	etcdDialTimeout = 5 * time.Second
	etcdGetTimeout  = 2 * time.Second
)

// Coordinator maintains the connection to the etcd cluster.
type Coordinator struct {
	sync.Mutex
	config     Config
	cli        *clientv3.Client
	log        zerolog.Logger
	healthOnly bool

	queryListeners    []coordinator.QueryListener
	presenceListeners []coordinator.NodeConfigListener
	guiListeners      []coordinator.GuiConfigListener
}

// NewCoordinator makes a new etcd controller.
func NewCoordinator(config Config, healthOnly bool) *Coordinator {
	c := &Coordinator{config: config, log: l.Logger.With().Str("component", "etcd").Logger(), healthOnly: healthOnly}
	return c
}

// RegisterQueryListener adds a QueryListener to the controller. When a new query is emitted
// by etcd, it calls IncomingQuery on each registered listener.
func (c *Coordinator) RegisterQueryListener(ql coordinator.QueryListener) {
	c.Lock()
	defer c.Unlock()
	c.queryListeners = append(c.queryListeners, ql)
}

// RegisterNodeConfigListener adds a NodeConfigListener to the controller. When a new node configuration is emitted
// by etcd, it calls NodeConfigurationUpdate on each registered listener.
func (c *Coordinator) RegisterNodeConfigListener(nl coordinator.NodeConfigListener) {
	c.Lock()
	defer c.Unlock()
	c.presenceListeners = append(c.presenceListeners, nl)
}

// RegisterGuiConfigListener adds a GuiConfigListener to the controller. When a new GUI configuration is emitted
// by etcd, it calls GuiConfigurationUpdate on each registered listener.
func (c *Coordinator) RegisterGuiConfigListener(nl coordinator.GuiConfigListener) {
	c.Lock()
	defer c.Unlock()
	c.guiListeners = append(c.guiListeners, nl)
}

func (c *Coordinator) RegisterAuditLogEntryListener(alel coordinator.AuditLogEntryListener) {
	c.log.Error().Msg("RegisterAuditLogEntryListener: Not implemented")
	return
}

func (c *Coordinator) Writer() io.Writer {
	// TODO: implement this method, do we need this?
	return ioutil.Discard
}

func (c *Coordinator) ConnectAndListen() error {
	// TODO: reconnect on error
	if err := c.connect(); err != nil {
		return err
	}

	if c.healthOnly {
		return nil
	}

	if err := c.subscribeEvents(); err != nil {
		return err
	}

	if err := c.dispatchMostRecentNodeConfigEvents(); err != nil {
		return err
	}

	if err := c.dispatchMostRecentGuiConfigEvents(); err != nil {
		return err
	}

	return nil
}

func (c *Coordinator) connect() error {
	// Setup TLS config
	var tlsConfig *tls.Config = nil
	if c.config.EtcdCACert != "" && c.config.EtcdClientCert != "" && c.config.EtcdClientKey != "" {
		c.log.Info().Msg("Starting etcd client with TLS")

		cert, err := tls.LoadX509KeyPair(c.config.EtcdClientCert, c.config.EtcdClientKey)
		if err != nil {
			c.log.Error().Err(err).Msg("Failed to load X509 Keypair")
			return err
		}

		caCertPool := x509.NewCertPool()
		caCert, err := ioutil.ReadFile(c.config.EtcdCACert)
		if err != nil {
			c.log.Error().Err(err).Msg("Failed to read TLS Certificate")
			return err
		}
		if ok := caCertPool.AppendCertsFromPEM(caCert); !ok {
			c.log.Error().Msg("Failed to parse TLS Certificate")
			return errors.New("Failed to parse TLS Certificate")
		}

		tlsConfig = &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
		}
	} else {
		c.log.Info().Msg("Starting etcd client")
	}

	// Create client (this does not try to connect yet)
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{fmt.Sprintf("%s:%s", c.config.EtcdHost, c.config.EtcdPort)},
		DialTimeout: etcdDialTimeout,
		TLS:         tlsConfig,
	})
	if err != nil {
		c.log.Error().Err(err).Msg("Failed to create etcd client")
		return err
	}

	// Try to connect to the etcd cluster with the client
	ctx, cancel := context.WithTimeout(context.Background(), etcdGetTimeout)
	_, err = cli.Get(ctx, "/")
	cancel()
	if err != nil {
		c.log.Error().Err(err).Msg("Failed to connect to the etcd cluster")
		return err
	}

	c.cli = cli
	return nil
}

// Close gracefully close the connection of the etcd client
func (c *Coordinator) Close() {
	c.Lock()
	defer c.Unlock()
	if c.cli != nil {
		c.cli.Close()
	}
}

// UpdateNodeConfig implements the listener interface of the coordinator
// Function is called when a node configuration update occurs
func (c *Coordinator) UpdateNodeConfig(ip, port, cert, paillierkey string) error {
	return c.setMPCNodeConfig(ip, port, cert, paillierkey)
}

// UpdateGuiConfig implements the listener interface of the coordinator
// Function is called when a GUI configuration update occurs
func (c *Coordinator) UpdateGuiConfig(ip, cert string) error {
	return c.setGUINodeConfig(ip, cert)
}

// UpdateHealth implements the listener interface of the coordinator
// Function is called when a Health update occurs
func (c *Coordinator) UpdateHealth(nodeName string, healthy bool, err error) error {
	if healthy {
		return c.setHealth(nodeName, "Reachable")
	}
	return c.setHealth(nodeName, fmt.Sprintf("Not reachable: %s", err.Error()))
}

// SubmitNewQuery attempts to submit the given query to the etcd coordinator.
// On success, it returns the new query's assigned ID.
func (c *Coordinator) SubmitNewQuery(newQuery types.Query, metadata coordinator.NewQueryMetadata) (string, error) {
	// TODO: Do we want to validate the queries here?
	id, err := randomID(32)
	if err != nil {
		return "", err
	}
	query := types.TaggedQuery{
		ID:    id,
		Query: &newQuery,
	}

	payload, err := JSONMarshal(query)
	if err != nil {
		return "", err
	}
	// TODO: remove hardcoded basicstats
	err = c.setQuery("basicstats", query.ID, string(payload))
	if err != nil {
		return "", err
	}
	return query.ID, nil
}

func (c *Coordinator) GetOrganizations() ([]string, error) {
	c.log.Error().Msg("Unimplemented: GetNodeConfig")
	return nil, errors.New("Unimplemented")
}

func (c *Coordinator) GetNodeConfig(organization string) (*coordinator.LogEntryPresenceMPC, error) {
	c.log.Error().Msg("Unimplemented: GetNodeConfig")
	return nil, errors.New("Unimplemented")
}

func (c *Coordinator) GetGuiConfig(organization string) (*coordinator.LogEntryPresenceGUI, error) {
	c.log.Error().Msg("Unimplemented: GetGuiConfig")
	return nil, errors.New("Unimplemented")
}

func (c *Coordinator) GetBusinessRules() []string {
	return []string{"GetBusinessRules: not implemented"}
}

func (c *Coordinator) DPaillierLock(context.Context) error {
	return errors.New("Unimplemented")
}
func (c *Coordinator) DPaillierUnlock(context.Context, string) error {
	return errors.New("Unimplemented")
}
func (c *Coordinator) DPaillierRead(context.Context) (string, common.Address, error) {
	return "", common.Address{}, nil
}
