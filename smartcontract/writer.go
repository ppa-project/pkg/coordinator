// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"encoding/json"
)

// Writer is the object that allows Zerolog to write log events
// to the coordinator. It implements io.Writer for this purpose.
type Writer struct {
	c chan OutgoingLog
}

// Write takes a byte array, which is assumed to be a json-encoded log message.
// It attempts to parse it as JSON, looking for fields relevant to the coordinator;
// if parsing fails, the error is raised. If parsing succeeds, Write succeeds
// whether anything is logged to the coordinator or not.
func (sw *Writer) Write(jsonBlob []byte) (int, error) {
	// Inspect the log entry as a string mapping, to see if relevant fields are
	// present
	var logEntry OutgoingLog
	if err := json.Unmarshal(jsonBlob, &logEntry); err != nil {
		return len(jsonBlob), nil
	}

	// We would like log entries that contain a query result or status. These contain
	// contain a key "queryid" and either "resulthash", "reviewer" or "aborted".
	if logEntry.Contains("queryid") && logEntry.ContainsAny([]string{"resulthash", "reviewer", "aborted"}) {
		sw.c <- logEntry
	}

	return len(jsonBlob), nil
}

type OutgoingLog map[string]interface{}

func (le *OutgoingLog) ContainsAny(keys []string) bool {
	for i := range keys {
		if le.Contains(keys[i]) {
			return true
		}
	}
	return false
}

func (le *OutgoingLog) Contains(key string) bool {
	_, ok := (*le)[key]
	return ok
}

func (le *OutgoingLog) Get(key string) interface{} {
	return (*le)[key]
}

func (le *OutgoingLog) GetOr(key string, fallback string) interface{} {
	if value, ok := (*le)[key]; ok {
		return value
	} else {
		return fallback
	}
}
