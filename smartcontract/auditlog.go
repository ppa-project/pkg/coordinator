// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"sort"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/rs/zerolog/log"
)

type LogEntryer interface {
	LogEntry(*Coordinator) coordinator.LogEntry
}

// AuditLogs is the main audit log cache.
type AuditLogs struct {
	// Mutex protecting the inner fields
	sync.Mutex
	// A goroutine sends smartcontract events from the individual channels
	// to the Entries slice. Sending on this channel ends that goroutine,
	// allowing the individual channels to be dropped.
	ChannelCloser chan struct{}
	// The cache of logentries. Is updated live using smartcontract
	// subscriptions
	Entries []coordinator.LogEntry
}

// AuditLogLen reports how many audit logs exist for the specified filter.
// Note that the cache of audit logs is continuously refreshed, so this value
// is a lower bound on the number of available log entries.
// If the logbook is not (yet) initialized, this function returns 0.
func (scc *Coordinator) AuditLogLen(filter coordinator.LogKind) int {
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()

	if scc.auditLogs.Entries == nil {
		return 0
	}

	count := 0
	for i := range scc.auditLogs.Entries {
		if scc.auditLogs.Entries[i].Kind.Matches(filter) {
			count++
		}
	}
	return count
}

// AuditLogs returns audit logs from the Coordinator's cache matching the given
// filter. The first (start) entries are skipped, and a maximum of (count) entries
// are returned. If count is zero, there is no maximum.
// If the logbook is not (yet) initialized, this function returns a nil slice.
func (scc *Coordinator) AuditLog(filter coordinator.LogKind, start, count int) []coordinator.LogEntry {
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()

	if scc.auditLogs.Entries == nil {
		return nil
	}

	logs := make([]coordinator.LogEntry, 0, count)
	matches := 0
	for i := range scc.auditLogs.Entries {
		if scc.auditLogs.Entries[i].Kind.Matches(filter) {
			if matches >= start {
				logs = append(logs, scc.auditLogs.Entries[i])
			}
			matches++
		}
		if count != 0 && len(logs) == count {
			break
		}
	}
	return logs
}

// Initializes the cache of audit logs and adds subscriptions to the incoming event
// dispatchers.
// Currently does not guarantee that no audit events are missed between the iteration of
// past events and subscription to future events. Re-initializing will catch such events.
func (scc *Coordinator) InitializeAuditLogs() {
	log.Info().Msg("Starting audit log cache initialization")
	// Fill and sort the cache of past events
	cache := scc.fillAuditLogCache()
	sort.Slice(cache, func(i, j int) bool { return cache[i].Timestamp.Before(cache[j].Timestamp) })

	// Put the cache in place and subscribe to new events
	scc.Lock()
	defer scc.Unlock()
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()
	scc.auditLogs.Entries = cache
	if scc.auditLogs.ChannelCloser == nil {
		scc.addAuditLogSubscriptions()
	}
	log.Info().Msg("Audit logs ready")
}

func (scc *Coordinator) fillAuditLogCache() []coordinator.LogEntry {
	scc.Lock()
	if scc.smartContract == nil {
		log.Warn().Msg("Initializing audit log cache failed: smart contracts not (or no longer) available")
		scc.Unlock()
		return nil
	}

	cache := make([]coordinator.LogEntry, 0)

	// Collect logs of each type
	if it, err := scc.smartContract.FilterGUINodeRegistered(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past GUI node presence events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, PresenceGUINodeRegistered(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Presence GUI logs")
	}
	if it, err := scc.smartContract.FilterMPCNodeRegistered(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past MPC node presence events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, PresenceMPCNodeRegistered(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Presence MPC logs")
	}
	if it, err := scc.smartContract.FilterQueryApproved(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query approval events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, QueryQueryApproved(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Approved logs")
	}
	if it, err := scc.smartContract.FilterQueryRejected(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query rejection events for the audit logs")
	} else {
		for it.Next() {
			// LogEntry for QueryRejected calls into scc.RuleExplanation, which takes this
			// lock as well.
			scc.Unlock()
			cache = append(cache, QueryQueryRejected(*it.Event).LogEntry(scc))
			scc.Lock()
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Reejected logs")
	}
	if it, err := scc.smartContract.FilterQueryUnaffordable(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query unaffordability events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, QueryQueryUnaffordable(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Unaffordable logs")
	}
	if it, err := scc.smartContract.FilterQueryRateLimited(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query rate limit events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, QueryQueryRateLimited(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Rate Limited logs")
	}
	if it, err := scc.smartContract.FilterQueryManualReview(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query in manual review events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, QueryQueryManualReview(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Manual Review logs")
	}
	if it, err := scc.smartContract.FilterQueryComplete(nil); err != nil {
		log.Warn().Err(err).Msg("Can not filter past query completion events for the audit logs")
	} else {
		for it.Next() {
			cache = append(cache, QueryQueryComplete(*it.Event).LogEntry(scc))
		}
		log.Debug().AnErr("sc-iterator-error", it.Error()).Msg("Iteration finished for Query Complete logs")
	}

	scc.Unlock()
	return cache
}

// Assumes lock held on coordinator and member auditlogs
func (scc *Coordinator) addAuditLogSubscriptions() {
	if scc.smartContract == nil {
		log.Warn().Msg("Initializing audit log cache failed: smart contracts not (or no longer) available")
		return
	}

	var firstBlockNr uint64 = 1
	opts := &bind.WatchOpts{Start: &firstBlockNr}

	// Subscribe to logs of each type
	chanGUINodeRegistered := make(chan *sc.PresenceGUINodeRegistered, 0)
	if subs, err := scc.smartContract.WatchGUINodeRegistered(opts, chanGUINodeRegistered); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to GUI node presence events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanMPCNodeRegistered := make(chan *sc.PresenceMPCNodeRegistered, 0)
	if subs, err := scc.smartContract.WatchMPCNodeRegistered(opts, chanMPCNodeRegistered); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to MPC node presence events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryApproved := make(chan *sc.QueryQueryApproved, 0)
	if subs, err := scc.smartContract.WatchQueryApproved(opts, chanQueryApproved); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query approval events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryRejected := make(chan *sc.QueryQueryRejected, 0)
	if subs, err := scc.smartContract.WatchQueryRejected(opts, chanQueryRejected); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query rejection events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryUnaffordable := make(chan *sc.QueryQueryUnaffordable, 0)
	if subs, err := scc.smartContract.WatchQueryUnaffordable(opts, chanQueryUnaffordable); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query unaffordability events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryRateLimited := make(chan *sc.QueryQueryRateLimited, 0)
	if subs, err := scc.smartContract.WatchQueryRateLimited(opts, chanQueryRateLimited); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query rate limit events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryManualReview := make(chan *sc.QueryQueryManualReview, 0)
	if subs, err := scc.smartContract.WatchQueryManualReview(opts, chanQueryManualReview); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query in manual review events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}
	chanQueryComplete := make(chan *sc.QueryQueryComplete, 0)
	if subs, err := scc.smartContract.WatchQueryComplete(opts, chanQueryComplete); err != nil {
		log.Warn().Err(err).Msg("Can not subscribe to query completion events for the audit logs")
	} else {
		scc.subscriptions = append(scc.subscriptions, subs)
	}

	// I desperately want generics, or even for Go channels to conform to some kind interface
	// or to allow type conversion based on inner type compatibility, but noooo
	scc.auditLogs.ChannelCloser = make(chan struct{}, 0)
	go func() {
		for {
			select {
			case event := <-chanGUINodeRegistered:
				scc.addEntryToAuditLog(PresenceGUINodeRegistered(*event).LogEntry(scc))
			case event := <-chanMPCNodeRegistered:
				scc.addEntryToAuditLog(PresenceMPCNodeRegistered(*event).LogEntry(scc))
			case event := <-chanQueryApproved:
				scc.addEntryToAuditLog(QueryQueryApproved(*event).LogEntry(scc))
			case event := <-chanQueryRejected:
				scc.addEntryToAuditLog(QueryQueryRejected(*event).LogEntry(scc))
			case event := <-chanQueryUnaffordable:
				scc.addEntryToAuditLog(QueryQueryUnaffordable(*event).LogEntry(scc))
			case event := <-chanQueryRateLimited:
				scc.addEntryToAuditLog(QueryQueryRateLimited(*event).LogEntry(scc))
			case event := <-chanQueryManualReview:
				scc.addEntryToAuditLog(QueryQueryManualReview(*event).LogEntry(scc))
			case event := <-chanQueryComplete:
				scc.addEntryToAuditLog(QueryQueryComplete(*event).LogEntry(scc))
			case <-scc.auditLogs.ChannelCloser:
				return
			}
		}
	}()
}

func (scc *Coordinator) addEntryToAuditLog(entry coordinator.LogEntry) {
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()

	scc.auditLogs.Entries = append(scc.auditLogs.Entries, entry)
	scc.dispatchNewAuditLogEntry(entry)
}

// Feeds a new node event to each of the registered listeners
func (scc *Coordinator) dispatchNewAuditLogEntry(entry coordinator.LogEntry) {
	scc.Lock()
	defer scc.Unlock()
	for i := range scc.auditLogEntryListeners {
		scc.auditLogEntryListeners[i](entry)
	}
}
