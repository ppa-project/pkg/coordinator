// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"time"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
)

// awaitTransaction waits until the given transaction has been mined. It returns an error
// if mining has failed, the connection to the blockchain fails, or the context expires.
func (scc *Coordinator) awaitTransaction(ctx context.Context, hash common.Hash) error {
	// In case of the simulated backend, no receipt is made before a commit().
	if simulatedBackend, ok := scc.connection.(committer); ok {
		simulatedBackend.Commit()
	}
	return scc.awaitTransactionNoCommit(ctx, hash)
}

// awaitTransactionNoCommit does the actual awaiting in awaitTransaction. It is extracted
// for its usefulness in tests, when explicit testing with fake delays is wanted.
func (scc *Coordinator) awaitTransactionNoCommit(ctx context.Context, hash common.Hash) error {
	for {
		receipt, err := scc.connection.TransactionReceipt(ctx, hash)
		// The simulated backend misbehaves and returns no error even if r is nil
		// so we have to be extra careful here even with the (sound) real backend
		// We exit if the error is different from "Not Found (Yet)" -> success or
		// permanent failure. Not Found from the simulated backend is (nil, nil).
		if (err == nil && receipt != nil) || (err != nil && err != ethereum.NotFound) { // result OK || a real error
			return err
		}

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(time.Second / 2):
		}
	}
}
