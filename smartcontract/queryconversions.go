// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"github.com/ethereum/go-ethereum/common"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"

	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

type PPAQueryQuery sc.PPAQueryQuery
type PPAQueryQueryMetadata sc.PPAQueryQueryMetadata

func (query PPAQueryQuery) TypesTaggedQuery(id *[32]byte) types.TaggedQuery {
	nq := query.TypesQuery()
	return types.TaggedQuery{
		ID:    (*common.Hash)(id).Hex()[2:],
		Query: &nq,
	}
}

func (query PPAQueryQuery) TypesQuery() types.Query {
	nq := types.Query{
		Aggregates: make([]types.Aggregate, 0, len(query.Columns)),
		Filters:    make([]types.Filter, 0, len(query.Constraints)),
	}
	for i := range query.Columns {
		nq.Aggregates = append(nq.Aggregates, types.Aggregate{
			Function:  types.AggregateFunction(query.Columns[i].Func),
			Owner:     query.Columns[i].Owner,
			Attribute: query.Columns[i].Attribute,
		})
	}
	for i := range query.Constraints {
		nq.Filters = append(nq.Filters, types.Filter(query.Constraints[i]))
	}
	return nq
}

func (metadata PPAQueryQueryMetadata) CoordinatorMetadata() coordinator.QueryMetadata {
	cm := coordinator.QueryMetadata{
		MinResultSize:         metadata.MinResultSize,
		MinResultPercentage:   metadata.MinResultPercentage,
		MaxResultPercentage:   metadata.MaxResultPercentage,
		PercentageConstraints: make([]coordinator.PercentageConstraint, 0, len(metadata.PercentageConstraints)),
		StdevConstraints:      make([]coordinator.StdevConstraint, 0, len(metadata.StdevConstraints)),
		Groupkey:              metadata.Groupkey,
		DestinationNodes:      metadata.DestinationNodes,
		Reviewer:              metadata.Reviewer,
	}
	for i := range metadata.StdevConstraints {
		cm.StdevConstraints = append(cm.StdevConstraints, PPAQueryStdevConstraint(metadata.StdevConstraints[i]).StdevConstraint())
	}
	for i := range metadata.PercentageConstraints {
		cm.PercentageConstraints = append(cm.PercentageConstraints, PPAQueryPercentageConstraint(metadata.PercentageConstraints[i]).PercentageConstraint())
	}
	return cm
}

func PPAQueryQueryFrom(nq *types.Query) sc.PPAQueryQuery {
	scq := sc.PPAQueryQuery{
		Columns:     make([]sc.PPAQueryColumn, 0, len(nq.Aggregates)),
		Constraints: make([]sc.PPAQueryConstraint, 0, len(nq.Filters)),
	}

	for i := range nq.Aggregates {
		scq.Columns = append(scq.Columns, sc.PPAQueryColumn{
			Func:      string(nq.Aggregates[i].Function),
			Owner:     nq.Aggregates[i].Owner,
			Attribute: nq.Aggregates[i].Attribute,
		})
	}
	for i := range nq.Filters {
		scq.Constraints = append(scq.Constraints, sc.PPAQueryConstraint(nq.Filters[i]))
	}
	return scq
}
