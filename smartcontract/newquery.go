// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"errors"
	"fmt"
	"time"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

var (
	topicQueryApproved     = "0x78e7d9e6fa0142259db294a13732f89f21f2c03e34cfcc807acb38f8d337be71"
	topicQueryComplete     = "0xf3800b8ad06ec32e97c88d30687ea98bb1085912fc361a6b873a0d68da91f0cc"
	topicQueryManualReview = "0x3a8507071141b32522a1bed4b0da5b417fa22b2685b87b830a44c42817c5b743"
	topicQueryRateLimited  = "0xd1605de0f4447dcfbd8c608c04c1f01abc6c0e271bf4bfd0dd5de588bfea1af8"
	topicQueryRejected     = "0x7008355febb6f598e6c8ec9136437eeb35cb2e63ff2a5796b246225d7f5a80b0"
	topicQueryUnaffordable = "0xbd2a3bd8b7504850a57472055068fd166ca1e8b9f0d407d33577d301a343c219"
)

// SubmitNewQuery attempts to submit the given query to the smart contract.
// On success, it returns the new query's assigned ID.
func (scc *Coordinator) SubmitNewQuery(newQuery types.Query, metadata coordinator.NewQueryMetadata) (string, error) {
	scc.Lock()
	defer scc.Unlock()
	if scc.Status != Connected || scc.smartContract == nil {
		return "", errors.New("not connected to the smart contract")
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	err := scc.ensureFunds(ctx)
	if err != nil {
		return "", errors.New("could not get funds for transacting on the blockchain: " + err.Error())
	}

	tx, err := scc.smartContract.SubmitQuery(scc.wallet, PPAQueryQueryFrom(&newQuery), sc.PPAQueryUserMetadata(metadata))
	if err != nil {
		return "", errors.New("query could not be checked by the smart contract: " + err.Error())
	}

	// Because the simulated backend doesn't make a receipt if you don't Commit()...
	if simulatedBackend, ok := scc.connection.(committer); ok {
		simulatedBackend.Commit()
	}

	// To get the query ID / error, we have to turn our transaction return value into a
	// receipt, from which we can parse the logs to find our query acceptance or error event
	receipt, err := scc.connection.TransactionReceipt(ctx, tx.Hash())
	triesRemaining := 20 // 10 seconds
	for err == ethereum.NotFound && triesRemaining != 0 {
		time.Sleep(500 * time.Millisecond)
		receipt, err = scc.connection.TransactionReceipt(ctx, tx.Hash())
		triesRemaining--
	}
	if err != nil {
		return "", errors.New("query processed by the smart contract, but could not retrieve transaction result: " + err.Error())
	}

	// Now, the logs might contain any of the events
	// Check all the logs (probably just one) in this receipt for our event
	hashes := ""
	for i := range receipt.Logs {
		if len(receipt.Logs[i].Topics) == 0 {
			continue
		}
		switch receipt.Logs[i].Topics[0].Hex() {
		case topicQueryApproved:
			eventApproved, err := scc.smartContract.ParseQueryApproved(*receipt.Logs[i])
			if err == nil {
				return common.Hash(eventApproved.QueryID).Hex()[2:], nil
			} else {
				return "", fmt.Errorf("query accepted by the smart contract; error parsing the Query Approved event: %v", err)
			}
		case topicQueryRejected:
			eventRejected, err := scc.smartContract.ParseQueryRejected(*receipt.Logs[i])
			if err == nil {
				return "", fmt.Errorf("query rejected by the smart contract for violation of rule %v", eventRejected.Rule)
			} else {
				return "", fmt.Errorf("query rejected by the smart contract for violation of a rule; error parsing the Query Rejected event: %v", err)
			}
		case topicQueryRateLimited:
			eventRateLimited, err := scc.smartContract.ParseQueryRateLimited(*receipt.Logs[i])
			if err == nil {
				errorString := "query rejected by the smart contract due to the rate limit: you previously submitted queries at "
				for i := range eventRateLimited.History {
					errorString += fmt.Sprintf("%v, ", time.Unix(eventRateLimited.History[i].Int64(), 0))
				}
				return "", errors.New(errorString[:len(errorString)-2])
			} else {
				return "", fmt.Errorf("query rejected by the smart contract due to the rate limit; error parsing the Query Rate Limited event: %v", err)
			}
		case topicQueryUnaffordable:
			eventUnaffordable, err := scc.smartContract.ParseQueryUnaffordable(*receipt.Logs[i])
			if err == nil {
				return "", fmt.Errorf("query rejected by the smart contract for exceeding the query budget: it costs %v and you have only %v",
					eventUnaffordable.Cost, eventUnaffordable.BudgetRemaining)
			} else {
				return "", fmt.Errorf("query rejected by the smart contract for exceeding the query budget; error parsing the Query Unaffordable event: %v", err)
			}
		}

		// Collect all hashes in case we fail
		for j := range receipt.Logs[i].Topics {
			hashes += fmt.Sprintf("\n%v", receipt.Logs[i].Topics[j].Hex())
		}
	}

	return "", fmt.Errorf("query processed by the smart contract, but the result was unreadable. Your node is probably not registered. Topics:\n%s", hashes)
}

type committer interface {
	Commit()
}
