// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

type PresenceGUINodeRegistered sc.PresenceGUINodeRegistered
type PresenceMPCNodeRegistered sc.PresenceMPCNodeRegistered
type QueryQueryApproved sc.QueryQueryApproved
type QueryQueryRejected sc.QueryQueryRejected
type QueryQueryUnaffordable sc.QueryQueryUnaffordable
type QueryQueryRateLimited sc.QueryQueryRateLimited
type QueryQueryManualReview sc.QueryQueryManualReview
type QueryQueryComplete sc.QueryQueryComplete

func (ev PresenceGUINodeRegistered) LogEntry(scc *Coordinator) coordinator.LogEntry {
	return coordinator.LogEntry{
		Kind:      coordinator.PresenceGUI,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryPresenceGUI{
			Organization: ev.NodeData.Organization,
			Ip:           ev.NodeData.Ip,
			Cert:         ev.NodeData.Cert,
		},
	}
}

func (ev PresenceMPCNodeRegistered) LogEntry(scc *Coordinator) coordinator.LogEntry {
	attributes := AttributesFromStrings(ev.NodeData.Attributes)

	return coordinator.LogEntry{
		Kind:      coordinator.PresenceMPC,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryPresenceMPC{
			Organization: ev.NodeData.Organization,
			Ip:           ev.NodeData.Ip,
			Port:         ev.NodeData.Port,
			Cert:         ev.NodeData.Cert,
			Paillierkey:  ev.NodeData.Paillierkey,
			Attributes:   attributes,
			DbHash:       ev.NodeData.DbHash,
		},
	}
}

func (ev QueryQueryApproved) LogEntry(scc *Coordinator) coordinator.LogEntry {
	return coordinator.LogEntry{
		Kind:      coordinator.QueryApproved,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryApproved{
			Sender:   ev.Sender,
			Query:    PPAQueryQuery(ev.Query).TypesTaggedQuery(&ev.QueryID),
			Metadata: PPAQueryQueryMetadata(ev.Metadata).CoordinatorMetadata(),
		},
	}
}

func (ev QueryQueryRejected) LogEntry(scc *Coordinator) coordinator.LogEntry {
	reason, err := scc.RuleExplanation(ev.Rule)
	if err != nil {
		reason = fmt.Sprintf("(%s)", err.Error())
	}
	return coordinator.LogEntry{
		Kind:      coordinator.QueryRejected,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryRejected{
			Sender: ev.Sender,
			Query:  PPAQueryQuery(ev.Query).TypesQuery(),
			Reason: reason,
		},
	}
}

func (ev QueryQueryUnaffordable) LogEntry(scc *Coordinator) coordinator.LogEntry {
	return coordinator.LogEntry{
		Kind:      coordinator.QueryUnaffordable,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryUnaffordable{
			Sender:          ev.Sender,
			Query:           PPAQueryQuery(ev.Query).TypesQuery(),
			Cost:            ev.Cost.Int64(),
			BudgetRemaining: ev.BudgetRemaining.Int64(),
		},
	}
}

func (ev QueryQueryRateLimited) LogEntry(scc *Coordinator) coordinator.LogEntry {
	history := make([]time.Time, len(ev.History))
	for i := range ev.History {
		history[i] = time.Unix(ev.History[i].Int64(), 0)
	}

	return coordinator.LogEntry{
		Kind:      coordinator.QueryRateLimited,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryRateLimited{
			Sender:  ev.Sender,
			Query:   PPAQueryQuery(ev.Query).TypesQuery(),
			History: history,
		},
	}
}

func (ev QueryQueryManualReview) LogEntry(scc *Coordinator) coordinator.LogEntry {
	return coordinator.LogEntry{
		Kind:      coordinator.QueryManualReview,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryManualReview{
			Sender:   ev.From,
			QueryID:  ev.QueryID,
			Reviewer: ev.Reviewer,
		},
	}
}

func (ev QueryQueryComplete) LogEntry(scc *Coordinator) coordinator.LogEntry {
	return coordinator.LogEntry{
		Kind:      coordinator.QueryComplete,
		Timestamp: scc.BlockNumberToTimestamp(ev.Raw.BlockNumber),
		Contents: coordinator.LogEntryQueryComplete{
			Sender:     ev.From,
			QueryID:    ev.QueryID,
			Successful: ev.ResultHash != "aborted",
			ResultHash: ev.ResultHash,
		},
	}
}

func (scc *Coordinator) BlockNumberToTimestamp(bn uint64) time.Time {
	block, err := scc.connection.BlockByNumber(context.Background(), new(big.Int).SetUint64(bn))
	if err != nil {
		return time.Time{}
	}

	return time.Unix(int64(block.Time()), 0)
}

// Generates the rule explanation string for a rule with a certain index.
func (scc *Coordinator) RuleExplanation(id *big.Int) (string, error) {
	scc.Lock()
	defer scc.Unlock()

	if scc.Status != Connected || scc.smartContract == nil {
		return "", fmt.Errorf("Error generating rule explanation for rule %v: smartcontract unavailable", id)
	}

	// Check that id is in [0, nRules), i.e. error if 0 < id || id !< nRules
	nRules, err := scc.smartContract.NRules(nil)
	if err != nil {
		return "", fmt.Errorf("Error retrieving number of rules: %s", err.Error())
	}
	if id.Cmp(big.NewInt(0)) == -1 || id.Cmp(nRules) != -1 {
		return "", fmt.Errorf("Error retrieving rule explanation for rule %v: index out of bounds [0, %v)", id, nRules)
	}

	format, err := scc.smartContract.DescribeRule(nil, id)
	if err != nil {
		return "", fmt.Errorf("Error generating rule explanation for rule %v: %s", id, err.Error())
	}
	data, err := scc.smartContract.GetRule(nil, id)
	if err != nil {
		return "", fmt.Errorf("Error generating rule explanation for rule %v: %s", id, err.Error())
	}

	// Convert []string to []interface{}, omitting empty parameters
	fmtParams := make([]interface{}, 0)
	for i := range data.Params {
		if len(data.Params[i]) == 0 {
			break
		}
		fmtParams = append(fmtParams, data.Params[i])
	}

	return fmt.Sprintf(format, fmtParams...), nil
}
