// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"

	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

// RegisterNodeTimeout is the timeout for node registrations
var RegisterNodeTimeout = 20 * time.Second

// Start connects to the blockchain, looks up the contracts we need and starts
// listening to the events it generates.
// When connected, it also sends the most recent MPC node configuration for each
// organization to each of the controller's registered NodeConfigListeners.
func (scc *Coordinator) ConnectAndListen() error {
	scc.Lock()
	if scc.Status == Connected || scc.Status == Connecting {
		scc.Unlock()
		return errors.New("Disconnect before trying to connect")
	}
	scc.Status = Connecting
	scc.Unlock()

	if err := scc.connect(); err != nil {
		scc.connectionError(err)
		return err
	}
	if err := scc.setupWallet(); err != nil {
		scc.connectionError(err)
		return err
	}

	if err := scc.subscribeEvents(); err != nil {
		scc.connectionError(err)
		return err
	}

	scc.Lock()
	scc.Status = Connected
	scc.Unlock()

	if err := scc.dispatchMostRecentNodeConfigEvents(); err != nil {
		scc.connectionError(err)
		return err
	}
	if err := scc.dispatchMostRecentGuiConfigEvents(); err != nil {
		scc.connectionError(err)
		return err
	}
	if err := scc.submitBackedUpLogs(); err != nil {
		scc.connectionError(err)
		return err
	}

	go scc.InitializeAuditLogs()

	return nil
}

// If needed, SetClient can be used to set a custom ethereum connection for the
// smart contract controller (e.g. for testing purposes).
// If client is nil, the controller's configuration will be used to make a connection
// to a real ethereum blockchain node.
func (scc *Coordinator) SetClient(client Connection) error {
	scc.Lock()
	defer scc.Unlock()
	var err error

	if _, ok := client.(*ethclient.Client); client == nil || ok {
		scc.connection, err = ethclient.Dial(scc.config.EthereumURL)
		if err != nil {
			log.Err(err).Str("url", scc.config.EthereumURL).Msgf("Failed to connect to the Ethereum client")
			return fmt.Errorf("failed to connect to the Ethereum client; %v", err)
		}
	} else {
		scc.connection = client
	}
	return nil
}

// Dials into the ethereum client and sets up the bindings for the smart contracts
func (scc *Coordinator) connect() error {
	// Make sure we have an ethclient

	// Set up the ethclient if needed
	if err := scc.SetClient(scc.connection); err != nil {
		return err
	}

	var scAddress common.Address

	if common.IsHexAddress(scc.config.EthereumContractAddress) {
		scAddress = common.HexToAddress(scc.config.EthereumContractAddress)
	} else {
		if addr := scc.FindFirstDiamondCutEvent(context.Background()); addr != nil {
			log.Info().
				Str("configured-address", scc.config.EthereumContractAddress).
				Stringer("found-address", addr).
				Msg("Smart contract address from configuration seems invalid; using first smartcontract we found on the blockchain")
			scAddress = *addr
			scc.config.EthereumContractAddress = scAddress.Hex()
		} else {
			log.Warn().
				Str("configured-address", scc.config.EthereumContractAddress).
				Str("configured-ethereum-url", scc.config.EthereumURL).
				Msg("Smart contract address from configuration seems invalid, but we can't find a deployed contract on the blockchain.")
		}
	}

	scc.Lock()
	defer scc.Unlock()

	// Instantiate the contracts
	presenceContract, err := sc.NewPresence(scAddress, scc.connection)
	if err != nil {
		log.Err(err).
			Str("address", scc.config.EthereumContractAddress).
			Msg("Failed to instantiate the Presence contract")
		return err
	}
	queryContract, err := sc.NewQuery(scAddress, scc.connection)
	if err != nil {
		log.Err(err).
			Str("address", scc.config.EthereumContractAddress).
			Msg("Failed to instantiate the Query contract")
		return err
	}
	maintenanceContract, err := sc.NewMaintenance(scAddress, scc.connection)
	if err != nil {
		log.Err(err).
			Str("address", scc.config.EthereumContractAddress).
			Msg("Failed to instantiate the Maintenance contract")
		return err
	}

	scc.smartContract = &SmartContract{
		Presence:    *presenceContract,
		Query:       *queryContract,
		Maintenance: *maintenanceContract,
	}
	return nil
}

// Sets up our address and makes sure there are some funds
func (scc *Coordinator) setupWallet() error {
	scc.Lock()
	defer scc.Unlock()

	// Generate a new private key for our own use
	privateKey, err := crypto.HexToECDSA(scc.config.EthereumPrivateKey)
	if err != nil {
		log.Err(err).
			Str("serialized private key", scc.config.EthereumPrivateKey).
			Msg("Unable to parse Ethereum private key")
		return err
	}

	scc.wallet = bind.NewKeyedTransactor(privateKey)
	return nil
}

func (scc *Coordinator) registerMPCNode() error {
	scc.Lock()
	defer scc.Unlock()
	ctx, cancel := context.WithTimeout(context.Background(), RegisterNodeTimeout)
	defer cancel()

	err := scc.ensureFunds(ctx)
	if err != nil {
		return err
	}

	tx, err := scc.smartContract.RegisterMPCNode(scc.wallet, NewMPCRegistrationData(scc.config))
	if err != nil {
		log.Err(err).Str("smartcontract-address", scc.config.EthereumContractAddress).Msg("Failed to register our MPC node at the smart contract")
		return err
	}

	err = scc.awaitTransaction(ctx, tx.Hash())
	log.Err(err).
		Str("transaction", tx.Hash().Hex()).
		Stringer("ethereum-address", scc.wallet.From).
		Msg("Registration of our MPC node at the smart contract")
	return err
}

func (scc *Coordinator) registerGUINode() error {
	scc.Lock()
	defer scc.Unlock()
	ctx, cancel := context.WithTimeout(context.Background(), RegisterNodeTimeout)
	defer cancel()

	err := scc.ensureFunds(ctx)
	if err != nil {
		return err
	}

	tx, err := scc.smartContract.RegisterGUINode(scc.wallet, NewGUIRegistrationData(scc.config))
	if err != nil {
		log.Err(err).Str("smartcontract-address", scc.config.EthereumContractAddress).Msg("Failed to register our GUI node at the smart contract")
		return err
	}

	err = scc.awaitTransaction(ctx, tx.Hash())
	log.Err(err).
		Str("transaction", tx.Hash().Hex()).
		Stringer("ethereum-address", scc.wallet.From).
		Msg("Registration of our GUI node at the smart contract")
	return err
}

// Close closes the underlying smart contract connection and releases all resourses.
func (scc *Coordinator) Close() {
	scc.Lock()
	defer scc.Unlock()
	scc.auditLogs.Lock()
	defer scc.auditLogs.Unlock()
	scc.innerClose()
}

func (scc *Coordinator) innerClose() {
	if scc.Status == Disconnected || scc.Status == Connecting {
		return
	}
	log.Info().Msg("smartcontract: Disconnecting")
	scc.Status = Disconnected
	scc.unsubscribeAll()
	if scc.auditLogs.ChannelCloser != nil {
		scc.auditLogs.ChannelCloser <- struct{}{}
		scc.auditLogs.ChannelCloser = nil
	}
	scc.connection.Close()
	scc.smartContract = nil
}

// connectionError is called if the controller learns there is a connection problem.
// It needs to release the current connection's resources, and schedule a reconnection.
func (scc *Coordinator) connectionError(err error) {
	scc.Lock()
	defer scc.Unlock()
	// Remove the current connection - if it's already gone, we've been called multiple times
	if scc.Status == Disconnected || scc.Status == WaitingToReconnect {
		return
	}
	log.Err(err).Msg("smartcontract: Connection error - starting reconnection")

	// Remove any subscriptions not yet errored out and close the connection
	scc.innerClose()
	scc.Status = WaitingToReconnect
	// Try to reconnect
	go scc.waitThenReconnect()
}

// Wait a while, then try to reconnect. Blocks.
func (scc *Coordinator) waitThenReconnect() {
	log.Info().Msg("smartcontract: Scheduling reconnection one minute from now")
	time.Sleep(time.Minute)

	// If the connection has been closed in this minute, abort

	scc.Lock()
	if scc.Status != WaitingToReconnect {
		scc.Unlock()
		return
	}
	scc.Unlock()

	log.Info().Msg("smartcontract: Attempting to reconnect")
	scc.ConnectAndListen()
}
