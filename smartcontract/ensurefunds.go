// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"math/big"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
)

// If the balance is low, top it up using the 'volunteer' account
// Must be called with scc.Mutex locked (which you should probably do anyway).
func (scc *Coordinator) ensureFunds(ctx context.Context) error {
	// Return immediately if the blockchain is Quorum based
	// TODO: can we automatically detect this?
	if viper.GetBool("quorum") {
		return nil
	}

	// Check the balance
	balance, err := scc.connection.BalanceAt(context.Background(), scc.wallet.From, nil)
	if err != nil {
		log.Err(err).
			Str("address", scc.wallet.From.Hex()).
			Msgf("Unable to check current balance of our wallet")
		return err
	}

	// Return early if we have enough balance (1e59 wei)
	target := big.NewInt(10)
	target = target.Exp(target, big.NewInt(59), nil)
	if balance.Cmp(target) > 0 {
		return nil
	}

	log.Info().
		Str("address", scc.wallet.From.Hex()).
		Msgf("Insufficient balance (%v), going to get more from the volunteer", balance)

	// Find the secret key of the 'volunteer' account
	volPrivateKeyString, err := scc.smartContract.Presence.VolunteerPrivateKey(nil)
	if err != nil {
		log.Err(err).Msgf("Unable to retrieve current volunteer private key")
		return err
	}
	volPrivateKey, err := crypto.HexToECDSA(
		fmt.Sprintf("%064s", volPrivateKeyString), // Zero padding to 64-char length
	)
	if err != nil {
		log.Err(err).Msgf("Unable to convert volunteer private key %s to an ECDSA key", volPrivateKeyString)
		return err
	}

	// Get the public key corresponding to it
	volPublicKey := volPrivateKey.Public()
	publicKeyECDSA, ok := volPublicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Error().Msgf("Unable to cast public key to ECDSA (was %v)", volPublicKey)
		return err
	}

	// Get the associated address and its current nonce
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := scc.connection.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Err(err).Msgf("Unable to retrieve pending nonce for volunteer address %v", fromAddress)
		return err
	}

	// Set the value and gas parameters
	value := big.NewInt(10)
	value = value.Mul(value, target) // 1e60 wei (volunteer starts with 9e74)
	gasLimit := uint64(21000)        // in units
	gasPrice, err := scc.connection.SuggestGasPrice(context.Background())
	if err != nil {
		log.Err(err).Msg("Unable to get suggested gas price")
		return err
	}
	toAddress := scc.wallet.From // Send ethers to our own wallet

	// Set up the transaction
	var data []byte
	tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, data)

	// Sign the transaction
	chainID, err := scc.connection.NetworkID(context.Background())
	if err != nil {
		log.Err(err).Msg("Unable to retrieve Network ID")
		return err
	}
	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), volPrivateKey)
	if err != nil {
		log.Err(err).Msg("Unable to sign transaction")
		return err
	}

	// Submit transaction to the network
	err = scc.connection.SendTransaction(ctx, signedTx)
	if err != nil {
		log.Err(err).Msgf("Unable to submit transaction")
		return err
	}

	log.Info().Str("transaction", signedTx.Hash().Hex()).Msg("Sent transaction to get funds from the volunteer, waiting for it to be mined")
	err = scc.awaitTransaction(ctx, signedTx.Hash())
	log.Err(err).Str("transaction", signedTx.Hash().Hex()).Msg("EnsureFunds completed")
	// Wait for the TX to go through
	return err
}
