// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package coordinator

import (
	"encoding/json"
	"time"

	"github.com/ethereum/go-ethereum/common"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// LogEntry is the type of all log entries returned as Audit Logs.
type LogEntry struct {
	Kind      LogKind   `example:"1"`
	Timestamp time.Time `example:"2020-11-30T10:36:28Z"`

	// Contents is always one of the LogEntry___ types defined in this package
	// and always corresponds to the value of Kind
	Contents interface{}
}

// LogKind is a bitfield for the kinds of log messages. Each log entry only has
// one kind, but the methods in a Coordinator that retrieve logs can specify
// a combination of filters in order to get the desired combination of logs.
type LogKind uint64

func (k LogKind) Matches(filter LogKind) bool {
	return k&filter != 0
}

const Any LogKind = LogKind(^uint64(0))
const (
	PresenceGUI LogKind = 1 << iota
	PresenceMPC
	QueryApproved
	QueryRejected
	QueryUnaffordable
	QueryComplete
	QueryRateLimited
	QueryManualReview
)

// The log entry contents for a GUI node presence notification
type LogEntryPresenceGUI struct {
	Organization string `example:"ACME"`
	Ip           string `example:"2.106.3.94"`
	Cert         string `example:"-----BEGIN CERTIFICATE----- ... -----END CERTIFICATE-----"`
}

// The log entry contents for an MPC node presence notification
type LogEntryPresenceMPC struct {
	Organization string `example:"ACME"`
	Ip           string `example:"2.106.3.94"`
	Port         string `example:"443"`
	Cert         string `example:"-----BEGIN CERTIFICATE----- ... -----END CERTIFICATE-----"`
	Paillierkey  string `example:"{\"N\":8972389472389478 ....}"`
	Attributes   types.DatabaseAttributes
	DbHash       string `example:"861f4ec2bd03b200a0ed11e7156e1206ab47ee7da6fe964303f10c7764ad8d14"`
}

// The log entry contents for a Query Approved notification.
type LogEntryQueryApproved struct {
	Sender   common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	Query    types.TaggedQuery
	Metadata QueryMetadata
}

// The log entry contents for a Query Rejected notification.
type LogEntryQueryRejected struct {
	Sender common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	Query  types.Query
	Reason string `example:"Query does not comply to the businessrules"`
}

// The log entry contents for a Query Unaffordable notification.
type LogEntryQueryUnaffordable struct {
	Sender          common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	Query           types.Query
	Cost            int64 `example:"30"`
	BudgetRemaining int64 `example:"1000"`
}

// The log entry contents for a Query Unaffordable notification.
type LogEntryQueryRateLimited struct {
	Sender  common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	Query   types.Query
	History []time.Time `swaggertype:"string" example:"[\"1970-01-15T06:56:07Z\"]"`
}

// The log entry contents for a Query in Manual Review notification.
type LogEntryQueryManualReview struct {
	Sender   common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	QueryID  [32]byte       `swaggertype:"string" example:"be05377deca950d2338d501ec948f00361d203b3fb55815303d7e5d8a0690163"`
	Reviewer string         `example:"PartyC"`
}

// The log entry contents for a Query Complete notification.
type LogEntryQueryComplete struct {
	Sender     common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	QueryID    [32]byte       `swaggertype:"string" example:"be05377deca950d2338d501ec948f00361d203b3fb55815303d7e5d8a0690163"`
	Successful bool           `example:"true"`
	ResultHash string         `example:"8aa253608c981d8d9b3130e7f4d8a60c9b491d2d93a454475a505b91bb6f9b29"`
}

type LogEntryQueryCompleteJSON struct {
	Sender     common.Address `swaggertype:"string" example:"0xcbc0deda74565cce14ddd6d581d84970ef66e34d"`
	QueryID    string         `example:"be05377deca950d2338d501ec948f00361d203b3fb55815303d7e5d8a0690163"`
	Successful bool           `example:"true"`
	ResultHash string         `example:"8aa253608c981d8d9b3130e7f4d8a60c9b491d2d93a454475a505b91bb6f9b29"`
}

func (l LogEntryQueryComplete) MarshalJSON() ([]byte, error) {
	queryID := common.Hash(l.QueryID).Hex()[2:]

	res := LogEntryQueryCompleteJSON{
		Sender:     l.Sender,
		QueryID:    queryID,
		Successful: l.Successful,
		ResultHash: l.ResultHash,
	}

	return json.Marshal(&res)
}
