// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"fmt"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
)

// GetOrganizations returns the list of organizations (for which administrators exist, and hence,
// for which node registration events can possibly exist).
func (scc *Coordinator) GetOrganizations() ([]string, error) {
	orgs, err := scc.smartContract.Organizations(nil)
	if err != nil {
		return nil, fmt.Errorf("Could not retrieve the list of registered organizations: %v", err)
	}
	return orgs, nil
}

// GetNodeConfig returns the most recent MPC node presence event for the given organization, or
// an error if none exist.
func (scc *Coordinator) GetNodeConfig(organization string) (*coordinator.LogEntryPresenceMPC, error) {
	nodeData, err := scc.smartContract.LatestApprovedMPCNodeData(nil, organization)
	if err != nil {
		return nil, fmt.Errorf("No presence notification found for organization %s: %v", organization, err)
	}

	logEntry := &coordinator.LogEntryPresenceMPC{
		Organization: nodeData.Organization,
		Ip:           nodeData.Ip,
		Port:         nodeData.Port,
		Cert:         nodeData.Cert,
		Paillierkey:  nodeData.Paillierkey,
		Attributes:   AttributesFromStrings(nodeData.Attributes),
		DbHash:       nodeData.DbHash,
	}

	return logEntry, nil
}

// GetNodeConfig returns the most recent GUI node presence event for the given organization, or
// an error if none exist.
func (scc *Coordinator) GetGuiConfig(organization string) (*coordinator.LogEntryPresenceGUI, error) {
	nodeData, err := scc.smartContract.LatestApprovedGUINodeData(nil, organization)
	if err != nil {
		return nil, fmt.Errorf("No presence notification found for organization %s: %v", organization, err)
	}

	logEntry := &coordinator.LogEntryPresenceGUI{
		Organization: nodeData.Organization,
		Ip:           nodeData.Ip,
		Cert:         nodeData.Cert,
	}

	return logEntry, nil
}
