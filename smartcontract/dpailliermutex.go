// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
)

// DPaillierLock takes the smart contract mutex, or gives an error if it is already taken.
// It supports a Context for timeout or cancellation. If you cancel the context, the mutex
// is left in an indeterminate state.
func (scc *Coordinator) DPaillierLock(ctx context.Context) error {
	scc.Lock()
	defer scc.Unlock()
	if err := scc.ensureFunds(ctx); err != nil {
		return err
	}

	tx, err := scc.smartContract.DPaillierTakeMutex(scc.wallet)
	if err != nil {
		return err
	}

	return scc.awaitTransaction(ctx, tx.Hash())
}

// DPaillierLock releases the smart contract mutex, or gives an error if it is not taken.
// newVersion is the new version string to be recorded as the DPaillier key version.
// It supports a Context for timeout or cancellation. If you cancel the context, the mutex
// is left in an indeterminate state.
func (scc *Coordinator) DPaillierUnlock(ctx context.Context, newVersion string) error {
	scc.Lock()
	defer scc.Unlock()
	if err := scc.ensureFunds(ctx); err != nil {
		return err
	}

	tx, err := scc.smartContract.DPaillierReleaseMutex(scc.wallet, newVersion)
	if err != nil {
		return err
	}

	return scc.awaitTransaction(ctx, tx.Hash())
}

// DPaillierRead ensures the blockchain is synchronized and then returns the version of
// the Paillier key registered in the smart contract. It also checks if the mutex is held
// and if so, returns the address of the holder. It supports a Context for timeout or
// cancellation.
func (scc *Coordinator) DPaillierRead(ctx context.Context) (string, common.Address, error) {
	scc.Lock()
	defer scc.Unlock()
	if err := scc.ensureFunds(ctx); err != nil {
		return "", common.Address{}, err
	}

	// Push the Button to ensure the blockchain node has synchronized
	tx, err := scc.smartContract.PushButton(scc.wallet)
	if err != nil {
		return "", common.Address{}, err
	}
	err = scc.awaitTransaction(ctx, tx.Hash())
	if err != nil {
		return "", common.Address{}, err
	}

	// Get the info
	callOpts := bind.CallOpts{
		Context: ctx,
	}

	version, err := scc.smartContract.DPaillierVersion(&callOpts)
	if err != nil {
		return "", common.Address{}, err
	}
	holder, err := scc.smartContract.DPaillierMutexStatus(&callOpts)
	if err != nil {
		return "", common.Address{}, err
	}

	return version, holder, nil
}
