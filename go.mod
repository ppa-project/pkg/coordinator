module ci.tno.nl/gitlab/ppa-project/pkg/coordinator

go 1.15

require (
	ci.tno.nl/gitlab/ppa-project/pkg/scbindings v1.0.14
	ci.tno.nl/gitlab/ppa-project/pkg/types v1.0.4
	github.com/ethereum/go-ethereum v1.9.25
	github.com/rs/zerolog v1.20.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	go.etcd.io/etcd v3.3.13+incompatible
	go.uber.org/zap v1.16.0 // indirect
	google.golang.org/grpc v1.22.0 // indirect
)

replace (
	ci.tno.nl/gitlab/ppa-project/pkg/scbindings => ci.tno.nl/gitlab/ppa-project/pkg/scbindings.git v1.0.14
	ci.tno.nl/gitlab/ppa-project/pkg/types => ci.tno.nl/gitlab/ppa-project/pkg/types.git v1.0.4
	github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.5
	github.com/ethereum/go-ethereum => ci.tno.nl/gitlab/ppa-project/pkg/go-ethereum.git v1.9.25-ppa-1
)
