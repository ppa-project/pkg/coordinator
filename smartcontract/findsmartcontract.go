// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"context"
	"fmt"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"

	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

// DiamondCutEventHash is the hash of the Diamond Cut event of the smart contract
var DiamondCutEventHash common.Hash

func init() {
	// Compute DiamondCutEventHash
	abi, err := abi.JSON(strings.NewReader(sc.DiamondFacetABI))
	if err != nil {
		panic(fmt.Sprintf("Can not parse the Diamond Facet ABI from scbindings: %v", err))
	}
	DiamondCutEventHash = abi.Events["DiamondCut"].ID
}

// FindFirstContractCreation queries the connected blockchain for the first contract creation
// transaction, and returns the address the contract is at.
// To prevent slowness, it only checks the first 10 blocks.
func (scc *Coordinator) FindFirstContractCreation() *common.Address {
	for blockNumber := int64(0); blockNumber != 10; blockNumber++ {
		block, err := scc.connection.BlockByNumber(context.Background(), big.NewInt(blockNumber))
		if err != nil {
			continue
		}
		for _, tx := range block.Transactions() {
			if tx.To() == nil {
				// Contract creation!
				receipt, err := scc.connection.TransactionReceipt(context.Background(), tx.Hash())
				if err == nil {
					return &receipt.ContractAddress
				}
			}
		}
	}
	return nil
}

// FindFirstDiamondCutEvent uses a custom filter on the DiamondCut event to find the first such
// event emitted on the blockchain by any contract. If any are found, the address of the contract
// that emitted it is returned.
func (scc *Coordinator) FindFirstDiamondCutEvent(ctx context.Context) *common.Address {
	// Filter all logs for this event
	query := ethereum.FilterQuery{Topics: [][]common.Hash{{DiamondCutEventHash}}}
	logs, _ := scc.connection.FilterLogs(ctx, query)
	if len(logs) != 0 {
		return &logs[0].Address
	}
	return nil
}
