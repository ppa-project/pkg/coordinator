// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package smartcontract

import (
	"encoding/hex"
	"math/big"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	sc "ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
)

// Test if node config updates are visible in the audit log
func TestGetQueryDetails(t *testing.T) {
	assert := assert.New(t)
	Setup()
	// Add a node that exists when we start up
	scPresence, _ := sc.NewPresence(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err := scPresence.NewAdmin(genesisAccount, "TestPartyA", genesisAccount.From)
	assert.Nil(err, "Can't add new admin to presence")
	client.Commit()

	_, err = scPresence.RegisterMPCNode(genesisAccount, sc.PPAStorageMPCNodeData{
		Organization: "TestPartyA",
		Ip:           "111",
		Port:         "115",
		Cert:         "222",
		Paillierkey:  "333",
		DbHash:       "555",
	})
	assert.Nil(err, "Can't register new node")
	client.Commit()
	_, err = scPresence.ApproveMPCNode(genesisAccount, genesisAccount.From)
	assert.Nil(err, "Can't approve new node")
	client.Commit()

	// Add a query event that exists when we start
	scMaintenance, _ := sc.NewMaintenance(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err = scMaintenance.SetPrivacyBudget(genesisAccount, "TestPartyA", big.NewInt(100000))
	assert.Nil(err, "Can't set privacy budget")
	scQuery, _ := sc.NewQuery(common.HexToAddress(testConfig.EthereumContractAddress), client)
	_, err = scQuery.SubmitQuery(genesisAccount, sc.PPAQueryQuery{
		Columns: []sc.PPAQueryColumn{{
			Func:      "SUM",
			Owner:     "A",
			Attribute: "A",
		}},
		Constraints: []sc.PPAQueryConstraint{{
			Owner:          "A",
			Attribute:      "A",
			Operator:       "==",
			ReferenceValue: "12",
		}},
	}, sc.PPAQueryUserMetadata{})
	assert.Nil(err, "Can't submit new query")
	client.Commit()

	// Make a controller with a node config listener
	c := NewCoordinator(*testConfig)
	c.SetClient(client)

	// Start our controller
	if c.ConnectAndListen() != nil {
		t.FailNow()
	}
	defer c.Close()
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// Get the query ID from the logs
	assert.Equal(1, c.AuditLogLen(coordinator.QueryApproved))
	logs := c.AuditLog(coordinator.QueryApproved, 0, 0)
	id := logs[0].Contents.(coordinator.LogEntryQueryApproved).Query.ID
	qd, err := c.GetQueryDetails(id)
	assert.Nil(err, "Error returned by GetQueryDetails")
	assert.Nil(qd.LogEntryQueryComplete, "Premature completion event from GetQueryDetails")
	assert.NotNil(qd.LogEntryQueryApproved, "No acceptance event found by GetQueryDetails")
	assert.Equal(*qd.LogEntryQueryApproved, logs[0].Contents.(coordinator.LogEntryQueryApproved))

	// Send query completion
	idBytes, err := hex.DecodeString(id)
	assert.Nil(err, "Hex error?")
	idArray := [32]byte{}
	copy(idArray[:], idBytes)
	_, err = scQuery.SubmitResult(genesisAccount, idArray, "hello")
	assert.Nil(err, "Can't submit result")
	client.Commit()
	time.Sleep(100 * time.Millisecond)

	// See if the logs are updated
	assert.Equal(1, c.AuditLogLen(coordinator.QueryComplete))
	qd, err = c.GetQueryDetails(id)
	assert.Nil(err, "Error returned by GetQueryDetails")
	assert.NotNil(qd.LogEntryQueryComplete, "No completion event found by GetQueryDetails")
	assert.NotNil(qd.LogEntryQueryApproved, "No acceptance event found by GetQueryDetails")
}
